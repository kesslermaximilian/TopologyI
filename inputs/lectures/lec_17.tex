%! TEX root = ../../master.tex
\lecture[Relative homology with coefficients. Functoriality of $H_{\chainbullet}(X;M)$. Mapping degree in homology with coefficients. Cellular chain complex with coefficients. Cohomology theories. Properties of cohomology theories: Triple sequences, Mayer Vietoris, suspension. Cochain complexes. Singular  cohomology. Exactness of  $\Hom_R(-,M)$ on free modules.]{Do 09 Dez 2021}{Cohomology}

\begin{dlemmadef}[Relative homology with coefficients]\label{def:relative-homology-with-coefficients}
    For a left $R$-module  $M$, and a pair of spaces $(X,A)$
   define the  \vocab{relative homology} with coefficients as
\[
    C_n(X,A;M) \coloneqq  \faktor{C_n(X;M)}{C_n(A;M)}
\] 
\[
    H_n(X,a;M) \coloneqq H_n(C_\chainbullet (X,A;M))
\] 
\end{dlemmadef}
\begin{proof}
    As in the case of singular homology,
    we get a short exact sequence
    \[
        0 \to  C_n(A;M) \hookrightarrow   C_n(X;M) \to C_n(X,A;M) \to 0
    \] 
    by definition, and the chain complex property follows.
\end{proof}

\begin{theorem}\label{thm:homology-with-coefficients-is-homology-theory}
    For every left $R$-module $M$,
    $H_\chainbullet (-;M)$ is a homology theory that satisfies
    the disjoint union and
    a modified version of the dimension axiom, i.e.
    \[
        H_n(\text{pt};M) \cong \begin{cases}
            M & n = 0 \\
            0 & \text{else}
        \end{cases}
    \] 
\end{theorem}
\begin{proof}[Sketch of proof]
    All proofs for singular homology go through by applying $- \otimes _{\mathbb{Z}} M$.
    This uses that a chain homotopy $h$ from  $f$ to  $g$
    becomes a chain homotopy  $h\otimes M$ from $f \otimes M$ to $g\otimes M$
    when applying $-\otimes _{\mathbb{Z}} M$.

    For the dimension axiom,
    note that
    \[
        C_*(\text{pt};M) = (\ldots \to  M \stackrel{0}{\longrightarrow} M
        \stackrel{\id}{\longrightarrow} M
        \stackrel{0}{\longrightarrow} M
        \to 0)
    \] 
    and thus
    \[
        H_n(\text{pt};M) = \begin{cases}
            M & n = 0 \\
            0 & \text{else}
        \end{cases}
    \] 
\end{proof}

\begin{corollary}\label{cor:freeness-of-relative-homotopy-of-skeletons-in-general-case}
    Let $(X,A)$ be a relative CW-complex.
    Then
    \[
        H_k(X_n, X_{n-1};M) \cong \begin{cases}
            \bigoplus_{\abs{I_n} } M & k = n \\
            0 & \text{else}
        \end{cases}
    \] 
\end{corollary}

\begin{lemma}\label{lm:functoriality-of-homology-with-coefficients-in-module}
    Let $M \stackrel{\varphi}{\longrightarrow} M'$ be an $R$-module homomorphism.
    Then there is a natural transformation
    $\varphi_*\colon H_n(-;M) \to H_n(-;M')$
    which is $\varphi$ on $H_0(\text{pt}; -)$.
\end{lemma}
\begin{proof}
   Similar to \autoref{exercise-9.1}.
\end{proof}
\begin{lemma}
    Let $f\colon S^n \to S^n$ be a map.
    Then
    \[
        M \cong H_n(S^n; M) \stackrel{f_*}{\longrightarrow} H_n(S^n;M) \cong M
    \] 
    is given by $m \mapsto \deg(f)\cdot m$.
\end{lemma}
\begin{remark*}
    We already claimed this for general $H$ (satisfying dimension axiom),
    but here this is easy to prove.
\end{remark*}
\begin{proof}
    Let $\mathbb{Z}\stackrel{\varphi_m}{\longrightarrow} M$
    be the map of abelian groups given my $1 \mapsto m$.
    Then the diagram
     \[
    \begin{tikzcd}
    \mathbb{Z}\cong  H_n(S^n;\mathbb{Z}) \ar[swap]{d}{{\varphi_{m}}_*} \ar{r}{f_*} & H_n(S^n;\mathbb{Z}) \cong \mathbb{Z} \ar{d}{{\varphi_m}_*} \\
      H_n(S^n;M) \ar[swap]{r}{f_*} & H_n(S^n;M)
    \end{tikzcd}
    \]
    which gives
    \[
    \begin{tikzcd}
        1  \ar[swap]{d}{} \ar{r}{} & \deg(f) \ar{d}{} \\
        m \ar[swap]{r}{} & \deg(f)\cdot %
    \end{tikzcd}
    \]
\end{proof}

\begin{corollary}\label{cor:cellular-homology-with-coefficients-arises-as-tensoring}
    The cellular chain complex for $H_*(X;M)$
    is given by $C_*^{\cell}(X) \otimes _{\mathbb{Z}} M$
    ( $\cong C_*^{\cell}(X) \otimes _{\mathbb{Z}} R \otimes _{R} M$)
\end{corollary}
\begin{proof}
    We have
    \[
        C_n^{\cell}(X) \otimes _{\mathbb{Z}} M 
        \cong \left( \bigoplus_{\abs{I_n} } \mathbb{Z}\right) \otimes _{\mathbb{Z}} M
    \] 
    and
    \[
        C_n^{\cell}(X;M) \coloneqq H_n^{\cell}(X_n, X_{n-1};M)
        \stackrel{\autoref{cor:freeness-of-relative-homotopy-of-skeletons-in-general-case}}{=} 
        \bigoplus_{\abs{I_n} } M
    \]
    so it remains to understand the $\partial$.

    For $C_n^{\cell}(X)$ and $C_n^{\cell}(X;M)$,
    the differential is given by the incidence matrix.
    Hence the chain complexes are isomorphic.
\end{proof}

\begin{example}\label{ex:cellular-homology-with-coefficients}
    \begin{enumerate}[1.]
        \item Let $n\geq 1$:
            \[
                H_k(S^n ;M) \cong \begin{cases}
                    M & k = 0,n \\
                    0 & \text{else}
                \end{cases}
            \] 
        \item
            \[
                H_k(\mathbb{C}\mathbb{P}^n;M) = \begin{cases}
                     M & k \leq 2n \text{ even} \\
                     0 & \text{else}
                \end{cases}
            \]
        \item
            \[
                H_k(\mathbb{R}\mathbb{P}^n; \faktor{\mathbb{Z}}{2}) \cong \begin{cases}
                    \faktor{\mathbb{Z}}{2} & k \leq n \\
                    0 & \text{else}
                \end{cases}
            \]
            The cellular chain complex of $\mathbb{R}\mathbb{P}^n$ is
            \[
            0 \to \mathbb{Z} \stackrel{\cdot 2 / 0}{\longrightarrow}
            \mathbb{Z} \to  \ldots
            \stackrel{0}{\longrightarrow} \mathbb{Z}
            \stackrel{\cdot 2}{\longrightarrow} \mathbb{Z}
            -\stackrel{0}{\longrightarrow} \mathbb{Z}
            \to 0
            \]
            Tensoring this with $\faktor{\mathbb{Z}}{2}$, we get
            \[
            0 \to \faktor{\mathbb{Z}}{2}
            \stackrel{0}{\longrightarrow} \mathbb{Z} \to \ldots
            \to \faktor{\mathbb{Z}}{2}
            \stackrel{0}{\longrightarrow} \faktor{\mathbb{Z}}{2}
            \to 0
            \] 
            so we see that all maps are trivial,
            and thus all homology groups below $n+1$ are  $\faktor{\mathbb{Z}}{2}$.
    \end{enumerate}
\end{example}

Instead of applying $-\otimes M$, we can also try to apply $\Hom_R(-;M)$.
But  note that $\Hom_R(-;M)$ is contravariant.

Hence we do not get a homology theory, but a cohomology theory.

\begin{lemma}\label{lm:degree-of-maps-multiplies-under-composition}
    Let $f,g\colon S^n \to  S^n$.
    Then $\deg(f \circ g) = \deg(f) \cdot \deg(g)$
\end{lemma}
\begin{proof}
    By functoriality,
    \[
        \begin{tikzcd}
            H_n(S^n ; \mathbb{Z}) \ar{r}{g_*} \ar[bend left = 20]{rr}{(f \circ g)_*}&
            H_n(S^n;\mathbb{Z}) \ar{r}{f_*} &  H_n(S^n;\mathbb{Z})
        \end{tikzcd}
    \]
    commutes and thus
    \[
        1 \mapsto \deg(g) \mapsto \deg(f)\cdot \deg(g)
    \] 
    yields our result.
\end{proof}


\section{Cohomology}
\begin{moral}
    Essentially, a cohomology theory is just a homology theory by inverting all arrows.
\end{moral}

\begin{definition}[Cohomology theory]\label{def:cohomology-theory}
    A \vocab{cohomology theory} $H^{\chainbullet}$ is a sequence of functors
    \[
        H^n\colon (\Top^2)^{\op} \to \Mod_R
    \]
    together with natural transformations
    \[
        δ\colon H^n(A) \to H^{n+1}(X,A)
    \]
    such that
    \begin{enumerate}[p]
        \item (Homotopy invariance)
            If $f,g\colon X \to Y$ are homotopic, then $f^* = g^*\colon H^n(Y) \to H^n(X)$ are identical.
        \item (Pair LES) $\forall (X,A)\in \Top^2$, the sequence
            \[
\ldots                \stackrel{\partial }{\longrightarrow} H^n(X,A) \to H^n(X) \to H^n(A) \stackrel{\partial }{\longrightarrow} H^{n+1}(X,A) \to \ldots
            \] 
            is exact.
        \item (Excision) Let $A\subset B\subset X$ satisfying $\overline{A}\subset \mathring{B}$.
            Then the inclusion induces an isomorphism.
            \[
                H^n(X,B) \stackrel{\cong}{\longrightarrow} H^n(X\setminus A, B\setminus A)
            \] 
        \item (Dimension Axiom) We have
            \[
                H^n(\text{pt} ) \cong \begin{cases}
                    R & n = 0 \\
                    0 & \text{else}
                \end{cases}
            \] 
        \item (Disjoint union) The inclusions $X_ \to \coprod_{i \in I}X_i$ induce
            an isomorphism
            \[
                H^n\left( \coprod_{i \in I}X_i \right) \to \prod_{i \in I}H^n(X_i)
            \] 
    \end{enumerate}
\end{definition}

\begin{oral}
    Note that the notion is not exactly dual, since the inclusion in the Disjoint union axiom
    remains an inclusion, and we had to replace the direct sum by a product.

    Also note that replacing the product with a direct sum will trivialize the homology theory,
    since taking $X_i = X$, for  $I$ infinite, one gets that the induced map has to be zero.
\end{oral}

\begin{theorem}\label{thm:triple-sequence-for-cohomology}
    For every triple of spaces $A\subset B\subset X$,
    there is a triple sequence
    \[
        \stackrel{δ}{\longrightarrow} H^n(X,B) \to H^n(X,A) \to H^n(B,A)
        \stackrel{δ}{\longrightarrow} H^{n+1}(X,B) \to  \ldots
    \] 
    with the boundary map $δ$ given by
     \[
         δ\colon H^n(B,A) \to H^n(B) \stackrel{δ}{\longrightarrow} H^{n+1}(X,B)
    \]
    and the others induced by their natural inclusions.
\end{theorem}

\begin{proof}
    As before in \autoref{lm:long-exact-sequence-of-triple-of-spaces}.
\end{proof}

\begin{theorem}\label{thm:mayer-vietoris-cohomology}
    Given a pushout
    \[
    \begin{tikzcd}
        X_0 \ar[swap]{d}{} \ar{r}{} & X_1 \ar{d}{} \\
        X_2 \ar[swap]{r}{} & X
    \end{tikzcd}
    \]
    with $H^n(X,X_2) \stackrel{\cong}{\longrightarrow} H^n(X_1,X_0)$ for all $n$,
    there is the Mayer-Vietoris sequence
    \[
        \ldots
        \stackrel{δ}{\longrightarrow} H^n(X)
        \stackrel{(j_1^*, j_2^*)}{\longrightarrow}  H^n(X_1) \oplus H^n(X_2)
        \stackrel{i_1^*-i_2^*}{\longrightarrow}  H^n(X_0) 
        \stackrel{δ}{\longrightarrow} \ldots
    \] 
\end{theorem}
\begin{proof}
    As before.
\end{proof}

\begin{theorem}\label{thm:suspension-isomorphism-for-cohomology}
    There is natural suspension isomorphism
    \[
        H^n(X, \left\{x\right\} ) \stackrel{\cong}{\longrightarrow}
        H^{n+1}\left(\sum X_j \left\{(x,0)\right\} \right)
    \] 
\end{theorem}

\begin{corollary}\label{cor:cohomology-of-sphere-for-dimension-axiom}
    If $H^{\chainbullet}$ satisfies the dimension axiom, then
    \[
        H^k(S^n) \cong \begin{cases}
            R & k = n,0 \\
            0 & \text{else}
        \end{cases}
    \] 
\end{corollary}

\begin{definition}\label{def:singular-cohomology}
    An \vocab{$R$-cochain complex}  $(C^{\chainbullet}, δ)$ is
    a sequence $C^n$ of  $R$-modules 
    with differentials $δ\colon C^n \to C^{n+1}$, such that $δ^2 = 0$.

    Its \vocab{cohomology} is defined as
    \[
        H^n(C^{\chainbullet}) =
        \faktor{\ker (C^n \stackrel{δ}{\longrightarrow} C^{n+1})}{\im (C^{n+1} \stackrel{δ}{\longrightarrow} C^n)}
    \]
\end{definition}

\begin{oral}
    The category of cochain complexes is equivalent to the category of chain complexes.
\end{oral}

\begin{definition}
    Let $M$ be an  $R$-module and  $(C_\chainbullet, \partial )$ an $R$-chain complex.
    Then the $R$-cochain complex  \vocab{$\Hom_R(C_\chainbullet ,M)$} is given by
    $\Hom_R(C_n,M)$ in degree  $n$,
    and differentials  $\hom_R(\partial, \id_M)$.
\end{definition}

\begin{oral}[Notation]
    It is common to denote homology theories by $H_\chainbullet $,
    i.e. have the star at the bottom, and cohomology theories by $H^{\chainbullet}$,
    i.e. have it at the top.

    Similarly, we use $\partial $ for the chain maps in homology, and
    $δ$ for the chain maps in cohomology by convention.
\end{oral}

\begin{definition}[Singular cohomology]
   Let $M$ be an  $R$-module.
   We define
   \[
       C^{\chainbullet}(X,A;M) \coloneqq \Hom_R(C_\chainbullet (X,A;R), M)
   \]
   and
   \[
       H^n(X,A;M) \coloneqq H^n(C^{\chainbullet}(X,A;M))
   \]
   $H^n(X,A;M)$ is the  $n^{\text{th}}$  \vocab{singular cohomology} of $(X,A)$ with coefficients. 
\end{definition}

\begin{oral}\label{oral:hom-to-module-functor-is-left-exact}
    Note that in general, $\Hom(-,M)$ is a left exact functor, that is,
    for a short exact sequence
     \[
    0 \to  A \to  B \to  C \to 0
    \] 
    we get an exact sequence
    \[
        0 \to \Hom(C,M) \to \Hom(B,M) \to \Hom(A,M)
    \]
\end{oral}

\begin{lemma}\label{lm:exactness-of-hom-for-free-modules}
    Let
    \[
    0 \to  F_0 \stackrel{f}{\longrightarrow} F_1 \stackrel{g}{\longrightarrow} F_2 \to 0
    \] 
    be a short exact sequence of $R$-modules.

    Assume that  $F_2$ is free,
    then also the induced sequence
    \[
        0
        \to  \Hom_R(F_2, M)
        \stackrel{g^*}{\longrightarrow} \Hom_R(F_1,M)
        \stackrel{f^*}{\longrightarrow} \Hom_R(F_0,M)
        \to 0
    \]
    is exact
\end{lemma}
\begin{proof}
    Since $\Hom_R(-,M)$ is left exact as mentioned in
    \autoref{oral:hom-to-module-functor-is-left-exact}
    (since $-\otimes _R M \leftadjoint \Hom_R(-,M)$ by an exercise),
    it suffices to show that $f^*$ is surjective.

    This is true since the sequence splits if $F_2$ is free.
\end{proof}

\begin{remark**}
    The splitting property of $F_2$ is just that we are asserting that
    free modules are so called \vocab{projective} modules. 
\end{remark**}

