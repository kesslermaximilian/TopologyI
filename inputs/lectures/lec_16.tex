%! TEX root = ../../master.tex
\lecture[Revision of cellular homology of $S^n$ and  $\mathbb{R}\mathbb{P}^n$. Product property of the Euler characteristic. Convex regular polyhedra. The Euler characteristic as the universal additive invariant on CW-complexes.]{Mi 08 Dez 2021}{Applications of the Euler characteristic}

\begin{orga}[Schedule of the next weeks]
    The next sessions will be:
    \begin{itemize}
        \item[13.12.] presentation of the bachelor thesis topics
        \item[15.12.] Q\&A session
        \item[16.12.] lecture
        \item[20.12.] lecture
        \item[22.12.] Q\&A
        \item[23.12.] free
    \end{itemize}
    Also, there will be no tutorials on $23.12.2021$ or  $07.01.2022$.
    Lectures will be held until the end of January.
\end{orga}

\begin{example*}
    We expand on
    \autoref{ex:homology-using-cellular-homology-of-sphere-and-complex-projective-space}.
    The cellular maps are given as
    \[
       f_i\colon  S^k \stackrel{\id}{\longrightarrow} 
        S^k \to 
        \faktor{S^k}{S^{k-1}} \stackrel{h_+ \twedge h_-}{\leftarrow}
        \faktor{D^k}{S^{k-1}} \twedge \faktor{D^k}{S^{k-1}} \stackrel{\pr_i}{\longrightarrow}
        \faktor{D^k}{S^{k-1}}
    \]
    where we denote
    \[
        h_+(x_1,\ldots.,x_k) = (x_1,\ldots,x_k, \sqrt{1- \norm{x} }), \qquad
        h_-(x_1,\ldots,x_k) = (x_1,\ldots,x_k, -\sqrt{1-\norm{x} })
    \]
    Then
    \[
        f_1(x_1,\ldots,x_{k+1}) = \begin{cases}
            (x_1,\ldots,x_k)& x_{k+1} \geq 0 \\
            \faktor{S^{k-1}}{S^{k-1}} & x_{k+1} \leq 0
        \end{cases}
    \]
    and similarly
    \[
        f_2(x_1,\ldots,x_{k+1}) = \begin{cases}
            \faktor{S^{k-1}}{S^{k-1}} & x_{k+1} \geq  0 \\
            (x_1,\ldots,x_k)& x_{k+1} \leq  0
        \end{cases}
    \]
    Consider
        \begin{equation*}
        g: \left| \begin{array}{c c l} 
        S^k & \longrightarrow & S^k \\
        (x_1,\ldots,x_{k+1}) & \longmapsto &  (x_1,\ldots,x_k, -x_{k+1})
        \end{array} \right.
    \end{equation*}
    Then $\deg(g) = -1$ recalling \autoref{lm:induced-homology-of-general-linear-map-of-sphere}
    and $f_2 = f_1 \circ g$.

    So the mapping degrees of $f_1$, $f_2$ will differ by a sign.
    Note that $f_1$ factors as
    \[
    \begin{tikzcd}[column sep = tiny]
        S^n \ar{rr}{f_1} \ar[swap]{dr}{p} & & \faktor{D^n}{S^{n-1}} \ar{dl}{\cong}[swap]{h_+} \\
    & \faktor{S^n}{D^n_-}
    \end{tikzcd}
    \]
    where $D^n_-$ denotes the lower hemisphere, and $p$ is an  $H_*$-isomorphism.

    Thus, $f_1$ is an $H_*$-isomorphism as well and it follows that
    for suitable isomorphism
    $\faktor{D^n}{S^{n-1}} \stackrel{\varphi}{\longrightarrow} S^n$ 
    we have $\deg (\varphi \circ  f_1) = 1$ and $\deg(\varphi \circ  f_2) = -1$.

    Thus, $\partial ^{\cell}$ is always given by the matrix
    \[
        \begin{pmatrix} 1 & 1 \\ -1 &-1 \end{pmatrix} , \qquad \begin{pmatrix} a \\ b \end{pmatrix}  \mapsto \begin{pmatrix} a + b \\ -a -b \end{pmatrix} 
    \]

    For $\mathbb{R}\mathbb{P}^n$ with $X_k = \mathbb{R}\mathbb{P}^k$,
    the cellular maps are given by
    \[
        \begin{tikzcd}
            S^{k} \ar[two heads]{r}{\pr} \ar{d}{\pr} \ar[bend right = 80,swap]{dd}{q^{k+1}}
            \ar{drr}{f} &
            \mathbb{R}\mathbb{P}^{k} \ar{r} &
            \faktor{\mathbb{R}\mathbb{P}^{k}}{\mathbb{R}\mathbb{P}^{k-1}} \ar{d}{\cong}\\
                \faktor{S^{k}}{x\sim -x} &
                \faktor{D^{k}}{\substack{x\sim -x \\ \forall x\in S^{k-1}} } \ar{r}
                \ar{l}{\cong}[swap]{h_+}& 
            \faktor{D^{k}}{S^{k-1}} \\
            \faktor{S^{k}}{S^{k-1}} &
            \faktor{D^k}{S^{k-1}} \twedge \faktor{D^k}{S^{k-1}}
            \ar{l}{\cong}[swap]{h_+ \twedge h_-}
            \ar[bend right = 20]{ur}[swap]{ \id \twedge -}
        \end{tikzcd}
    \]
    Then we have
    \[
        \deg(f) = \deg(f_1) + \deg(- \circ f_2)
    \] 
    and
    \[
    \begin{tikzcd}
        H_k\left( \faktor{D^k}{S^{k-1}}, \frac{S^{k-1}}{S^{k-1}} \right) \ar{d}[swap]{-}&
        H_{k-1}(D^k, S^{k-1}) \ar{l}[swap]{\cong} \ar{d} \ar{r}{\partial }[swap]{\cong} &
        \tilde{H}_{k-1}(S^{k-1}) \ar{d}[swap]{A}\\
        H_k(\faktor{D^k}{S^{k-1}}, \faktor{S^{k-1}}{S^{k-1}}) &
        H_k(D^k, S^{k-1}) \ar{l}[swap]{\cong} \ar{r}{\partial }[swap]{\cong} &
        \tilde{H}_{k-1}(S^{k-1})
    \end{tikzcd}
    \]
    so that
    \[
        \deg(-) = \deg(A_{S^{k-1}}) = (-1)^k, \quad \implies \quad \deg(f) = 1 + (-1)(-1)^{k} = 1 + (-1)^{k+1}
    \]

    We have to sum the degrees in this case, since
    \[
        \tilde{H}_k\left(\faktor{D^k}{S^{k-1}} \twedge \faktor{D^k}{S^{k-1}}\right)
        \cong
        \tilde{H}_k\left(\faktor{D^k}{S^{k-1}}\right)
        \oplus \tilde{H}_k\left(\faktor{D^k}{S^{k-1}}\right)
    \] 
\end{example*}

We now continue with the Euler characteristic.

\begin{lemma}\label{lm:product-of-finite-cw-complexes-multiplies-euler-characteristic}
    Let $X,Y$ be finite CW-complexes.
    Then
    \[
        \chi(X \times Y) = \chi(X) \cdot \chi(Y)
    \] 
\end{lemma}
\begin{proof}
    We have
    \[
        I_n(X\times Y) = \bigcup_{p + q = n} I_p(X) \times I_q(Y)
    \] 
    by recalling the proof that $X\times Y$ is a CW-complex.
    Thus
    \begin{IEEEeqnarray*}{rCl}
        \chi(X) \cdot \chi(Y) &
        = & \left( \sum_{p\geq 0} (-1)^p \abs{I_p(X)} \right)  \left( \sum_{q\geq 0} (-1)^q \abs{I_q(Y)}  \right)  \\
          & = & \sum_{p\geq 0} \sum_{q \geq 0} (-1)^{p+q} \abs{I_p(X)}  \abs{I_q(Y) } \\
          & = & \sum_{n\geq 0} (-1)^n \sum_{p+q=n} \abs{I_p(X)} \abs{I_q(Y)} \\
          & = & \sum_{n\geq 0} (-1)^n \abs{I_n(X\times Y)}  \\
          & = & \chi(X\times Y)
    \end{IEEEeqnarray*}
\end{proof}

\begin{example}[Computation of some Euler characteristics]
    Using \autoref{lm:product-of-finite-cw-complexes-multiplies-euler-characteristic} one gets:
    \begin{enumerate}[1.]
        \item For a finite CW-complex, we have
        \[
            \chi(X\times S^1) = 0
        \] 
        and in particular
        \[
            \chi(T^n) = 0
        \] 
    \item We have
        \[
            \chi(\mathbb{R}\mathbb{P}^n) = \sum_{p=0}^n (-1)^p = \begin{cases}
                0 & n \text{ odd} \\
                1 & n \text{ even}
            \end{cases}
        \] 
        \[
            \chi(\mathbb{C}\mathbb{P}^n) = \sum_{p=0}^n (-1)^{2p} = n + 1
        \] 
        \[
            \chi(\Sigma_g) = 1 - 2g + 1 = 2 - 2g
        \] 
    \end{enumerate}
\end{example}

\begin{example}[Convex regular polyhedra]\label{ex:convex-regular-polyhedra-or-platonic-solids}
    A \vocab{convex regular polyhedron} is a convex, compact subspace of $\mathbb{R}^3$
    bounded by regular, $n$-sided polygons whose intersection is either empty,
    a vertex or an edge such that each vertex has  $m$ edges.

    Examples are the tetrahedron, the cube.

    Define $V \coloneqq  \# \text{vertices}$,
    $E \coloneqq  \# \text{edges}$ and
    $F \coloneqq  \# \text{faces}$

    \begin{tabular}{c | c c c c c}
        Polyhedron & $m$ & $n$ & $V$ & $E$ & $F$ \\
        \hline
        Tetrahedron & 3 & 3 & 4 & 6 & 4 \\
        Hexahedron & 3 & 4 & 8 & 12 & 6 \\
        Octahedron & 4 & 3 & 6 & 12 & 8 \\
        Dodecahedron & 3 & 5 & 20 & 30 & 12 \\
        Icosahedron & 5 & 3 & 12 & 30 & 20
    \end{tabular}

    We claim that these are the only possible values
    \begin{oral}
        We will not be using the convex condition.
        So we do not claim that we prove that the above list is complete in terms of polyhedra
        (although it is), and in fact there are more non-convex shapes that share the same values
        of $m$, $n$, $V$, $E$, $F$.
    \end{oral}

    The surface of a regular convex polyhedron is $S^2$ and the polygons give a CW-structure.
    Hence $2 = \chi(S^2) = V - E + F$.
    Since $m$ edges meet in each vertex, we have  $mV = 2E$.
    Since each polygon has  $n$ sides, we have  $nF = 2E$.

    Plugging in yields
     \[
    2 = \frac{2}{m} E - E + \frac{2}{n}E
    \quad\iff\quad
    \frac{1}{E} + \frac{1}{2} = \frac{1}{m} + \frac{1}{n}
    \] 
    In particular, this gives $\frac{1}{m} + \frac{1}{n} > \frac{1}{2}$,
    and as $m,n>2$ we see that only the listed values of $m$, $n$ are possible.

    From $m$, $n$ we can then compute  $V$, $E$, $F$.
\end{example}

\begin{definition}[Additive invariant]\label{def:additive-invariant}
    An \vocab{additive invariant} $(A,a)$ of finite CW-complexes consists of
    an abelian group $A$ and an element $a(X)\in A$ for every finite Cw-complex $X$
    such that
    \begin{enumerate}[p]
        \item Homotopy invariance, i.e. $X \simeq Y \implies a(X) = a(Y)$.
        \item Additivity: Given a pushout
            \[
                \begin{tikzcd}[sep = large]
                    X_0 \ar[swap]{d}{\text{cellular}} \ar[hook]{r}{\text{subcomplex}} & X_1 \ar{d}{} \\
                X_2 \ar[swap]{r}{} & X
            \end{tikzcd}
            \]
            we have $a(X) = a(X_1) + a(X_2) - a(X_0)$
        \item Normalized, i.e. $a(\emptyset) = 0$.
    \end{enumerate}
\end{definition}

\begin{definition}\label{def:universal-additive-invariant}
    An additive invariant $(U,u)$ is  \vocab{universal} if
    for every additive invariant $(A,a)$, there exists a unique morphism
    $\varphi\colon U \to  A$ such that $\varphi(u(X)) = a(X)$ for all CW-complexes $X$.
\end{definition}

\begin{theorem}
    $(\mathbb{Z}, \chi)$ is the universal additive invariant.
\end{theorem}

\begin{proof}
    We first check that $\chi$ is an additive invariant.
    We already know $(i)$,  $(iii)$ is obvious.

    $(ii)$ follows from the proof of exercise 7.1.

    If $\varphi\colon \mathbb{Z} \to  A$ satisfies $\varphi(\chi(X)) = a(X)$,
    then $\varphi$ is unique with this property,
    because $\varphi$ is determined by $\varphi(1) = \varphi(\chi(\text{pt})) = a(\text{pt})$.

    So it is enough to show existence of such a $\varphi$.

    First note that additivity with $X_0 = \emptyset$ implies that (for $I$ finite)
    \[
        a\left( \coprod_{i \in I}X_i \right) = \sum _{i \in I}a(X_i)
    \] 

    We now show that $\varphi$ given by $\varphi(1) = a(\text{pt})$ satisfies
    $\varphi(\chi(X)) = a(X)$,
    this is done by induction on $\dim X = n$.

    For  $n=-1$,  $X = \emptyset$, and
    $\varphi(\chi(\emptyset)) = \varphi(0) = 0 = a(\emptyset)$ holds.

    For $n= 0$,  $X = \coprod _{i \in I} \text{pt}$,
    and thus
    \[
        \varphi\left(\chi(\coprod _{i \in I} \text{pt})\right)
        = \varphi(\abs{I})
        = \abs{I} \varphi (1)
        = \abs{I} a (\text{pt})
        = a \left( \coprod_{i \in I}\text{pt} \right)
    \] 

    For the induction step, take any choice of such a pushout
    \[
    \begin{tikzcd}
        \coprod_{i \in I}I_n  S^{n-1}\ar[swap]{d}{} \ar{r}{} & X_{n-1} \ar{d}{} \\
        \coprod _{i \in I}D^n \ar[swap]{r}{} & X_n
    \end{tikzcd}
    \]
    By additivity, we have
    \begin{IEEEeqnarray*}{rCl}
        a(X_n) & = & a(X_{n-1}) - a \left( \coprod _{i \in I} S^{n-1} \right)  + a \left( \coprod _{i \in I} D^n \right)  \\
               & \stackrel{\text{hom. inv.}}{=}  & a(X_{n-1}) - a\left( \coprod _{i \in I} S^{n-1} \right) + a \left( \coprod_{i \in I}I_n \text{pt} \right)  \\
               & \stackrel{\text{ind.}}{=}  & \varphi(\chi(X_{n-1})) - \varphi(\chi(\coprod _{i \in I}S^{n-1})) + \varphi(\chi(\coprod _{i \in I}\text{pt})) \\
               & = & \varphi(\chi(X_{n-1}) - \chi(\coprod _{i \in I}S^{n-1}) + \chi(\coprod _{i \in I}D^n)) \\
               & = & \varphi(\chi(X_n))
    \end{IEEEeqnarray*}
\end{proof}

\section{Homology with coefficients}
We consider a variant of singular homology.
For this, we fix a ring $R$ and a left $R$-module $M$.

The main example will be $R = \mathbb{Z}$ and $M$ as an abelian group.
Until now, we always considered  $R = M = \mathbb{Z}$.

\begin{lemmadef}\label{chains-with-coefficients}
    Let
    \[
        C_n(X, R) \coloneqq R[\left\{\Delta^n \to X\right\} ] = \bigoplus_{\Delta^n \to X} R
    \]
    As for $C_n^{\sing}(X)$, we have the boundary maps
        \begin{equation*}
        \partial : \left| \begin{array}{c c l} 
            C_n(X,R) & \longrightarrow & C_{n-1}(X, R) \\
            \sum_{i\in I} r_i σ_i  & \longmapsto &  \sum_{i\in I} \sum_{j=0}^n (-1)^j r_i \cdot (σ_i \circ δ^j)
        \end{array} \right.
    \end{equation*}

    Similarly, let
    \[
        C_n(X,M) \coloneqq C_n(X, R) \otimes _R M \cong \bigoplus_{\Delta^n \to X} M
    \]
    and denote
    \[
        \partial \coloneqq \partial ^r \otimes \id_M \colon  C_n(X,M) \to C_{n-1}(X,M)
    \] 

    Then $(C_\chainbullet (X,M),\partial )$ is a chain complex of left $R$-modules.
\end{lemmadef}

\begin{note}
    This could also be formulated as $C_\chainbullet (X, R) = C_\chainbullet ^{\sing}(X) \otimes _\mathbb{Z} R$.
\end{note}

\begin{ddefinition}\label{def:homology-with-coefficients}
    We define the \vocab{homology with coefficients} in $M$ as
    \[
        H_n(X, M) \coloneqq H_n(C_\chainbullet (X,M),\partial )
    \] 
\end{ddefinition}
