%! TEX root = ../../master.tex
\lecture[]{So 28 Nov 2021}{Untitled}

\begin{editor}
    The definition of the mapping cylinder in the last lecture has been adjusted
\end{editor}

\begin{definition*}
    The \vocab{mapping cone} of a map $f\colon X \to Y$ is given as
    \[
        C(f)
        \coloneqq
        \faktor{X \times I \cup _{X\times \left\{1\right\} } Y}{X \times \left\{0\right\} }
        \cong
        \faktor{M(f)}{X\times \left\{0\right\} }
    \] 
\end{definition*}

We had the statement that for an NDR-pair $(X,A)$, the map
 \[
     (X,A) \to (\faktor{X}{A}, \faktor{A}{A})
\] 
is an $H_*$-isomorphism.
This follows from  \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad} by the pushout
\[
\begin{tikzcd}
\pushout A \ar[swap]{d}{} \ar{r}{} & X \ar{d}{}  \ar{r} & (X,A) \ar{d}{\text{$H_*$-isomorphism}}\\
    \star \ar[swap]{r}{} & \faktor{X}{A}\ar{r} & \left(\faktor{X}{A}, \faktor{A}{A}\right)
\end{tikzcd}
\]

\begin{remark}
    \begin{enumerate}[1.]
        \item The CW-structure in \autoref{thm:product-of-cw-complex-is-cw-complex-if-one-is-locally-compact} is given by
            \[
                (X\times Y)_n = \bigcup_{l+k=n} X_l \times Y_k
            \] 
    \missingfigure{Illustration of product of $CW$-complexes}
        \item The quotient in the proof always gives a CW-complex $X \hat{\times } Y$ with a continuous bijection $X\hat{\times }Y \to  X\times Y$.

            But in general, this is not a homeomorphism.
            Sometimes one works in the category of compactly generated spaces to avoid this. In this category, the product of two CW-complexes is indeed a CW-complex.
    \end{enumerate}

\end{remark}

\begin{corollary}\label{cor:n-torus-is-cw-complex}
    The $n$-torus  $T^n = \prod_{i=1}^n S^1$ is a CW-complex.
\end{corollary}

\begin{proposition}\label{prop:relative-cw-is-gives-ndr-pair}
    Let $(X,A)$ be a relative  $CW$-complex.
    Then  $(X,A)$ is an NDR pair.
\end{proposition}

\begin{proof}
    We use the pushout
\[
\begin{tikzcd}
    \coprod_{I_n}S^{n-1} \ar[swap]{d}{} \ar{r}{} & X_{n-1} \ar{d}{} \\
    \coprod_{I_n}D^n \ar[swap]{r}{} & X_n
\end{tikzcd}
\]
Since $\left( \coprod_{I_n}D^n, \coprod_{I_n} S^{n-1} \right) $ is an NDR-pair, so is $(X_n, X_{n-1})$ by \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad}.

By an exercise \todo{reference}, this is equivalent to 
\[
    (X_n, X_{n-1})
\] 
having the homotopy extension property, i.e.
\[
\forall f\colon  X_n \to Y, \qquad h\colon X_{n-1}\times I \to Y
\] 
with $\frestriction{f}{X_{n-1}} = h_0$ there exists $H\colon X_n \times I \to Y$ with $f=H_0$ and  $\frestriction{H}{X_{n-1}\times I} = h$.

Recall that $A \coloneqq X_{-1}$.
Let $f\colon X \to  Y$ and a homotopy $h\colon  A \times I \to Y$ with $h_0 = \frestriction{f}{A}$ be given.
Then $h$ inductively extends to a homotopy on $X_n$ for all $n$. 

We thus get continuous maps
\[
\begin{tikzcd}[column sep = tiny]
    X_n\times I \ar{rr}{} \ar[swap]{dr}{h_n} & & X_{n+1}\times I \ar{dl}{h_{n+1}} \ar{r} & \ldots \ar[bend left = 40]{dll} \\
& Y
\end{tikzcd}
\]
As these maps commute, this yields (by taking colimits) a map $H\colon  X \times I \to Y$.
Then also $(X,A)$ has the homotopy extension property, and thus is an NDR-pair.

$X\times I$ has a CW-complex structure by \autoref{thm:product-of-cw-complex-is-cw-complex-if-one-is-locally-compact}.
We have
\[
\begin{tikzcd}[column sep = tiny]
    (X\times I)_n \ar{rr}{} \ar[swap]{dr}{\frestriction{H}{(X\times I)_n}} & & X_n\times I \ar{dl}{h_n} \\
& Y
\end{tikzcd}
\]
Hence $\frestriction{H}{(X\times I)_n}$ is continuous for all $n$ and thus $H$ is  continuous because $X\times I$ has the colimit topology.
\end{proof}

\begin{oral}
    The proof also showed that
    \[
    \colim X_n \times I = X_n
    \]
    since one has the commutative diagram
    \[
    \begin{tikzcd}[column sep = -2em]
      \ar[hook]{dr}       X_n \times I \ar{rr} & &\ar[hook]{dr} X_{n+1} \times I \ar{rr} & & X_{n+2} \times I \ar{rr} & &[4em] \ldots \\
                                                 & \ar[hook]{ur}(X\times I)_{n+1} \ar{rr} & & (X\times I)_{n+2} \ar{rr}\ar[hook]{ur} & & \ldots
    \end{tikzcd}
    \] 
\end{oral}

\begin{lemma}\label{lm:relative-cw-complex-is-hausdorff}
    If $(X,A)$ is a relative CW-complex, then  $X$ is Hausdorff.
\end{lemma}

\begin{oral}
    This is of course only true if $A$ is Hausdorff.
    For us, this is part of the definition of a  CW-complex being Hausdorff, but there are definitions that do not assume this.
\end{oral}

\begin{proof}
    We first show that $X_n$ is Hausdorff for all  $n$ and use induction.
    For  $n = -1$ we have that $X_{-1} = A$ is Hausdorff by assumption.

    For the induction step,
    \[
    X_n = X_{n-1} \cup _{I_n \times S^{n-1}} I_n \times D^n
    \] 
    Since $I_n \times \mathring{D}^n\subset X_n$ is an open subspace which is Hausdorff, $x,y\in I_n\times \mathring{D}^n$, $x\neq y$ can be separated by disjoint open neighborhoods.

    For $x\in \left\{i\right\} \times \mathring{D}^n\subset I_n \times \mathring{D}^n$ and $y\in X_{n-1}$, let $U$ be an open neighborhood of  $x$ in  $\left\{i\right\} \times D^n$ and $V'$ an open neighborhood of  $\left\{i\right\} \times S^{n-1} \subset \left\{i\right\} \times D^n$ with $U\cap V' = \emptyset$.
    (Note this is possible e.g. because in a Hausdorff space, we can separate any point from a compact subse. See \cite[Lemma 5.9]{geotopo}]) 

    Now take
    \[
        V \coloneqq X_{n-1} \cup (I_n \setminus \left\{i\right\} \times D^n) \cup V'
    \]
    Then $V\subset X_n$ is open and $V\cap U = \emptyset$, so these are separating neighborhoods of $x,y$.

    It remains the case where $x,y\in X_{n-1}$.
    Then consider the pushout
    \[
    \begin{tikzcd}
\pushout        I_n \times S^{n-1} \ar[swap]{d}{} \ar{r}{} & X_{n-1} \ar{d}{} \\
        I_n \times D^n \ar[swap]{r}{} & X_n
    \end{tikzcd}
    \]
    and take $X_{n-1} \cup I_n \times (D^n \setminus \left\{\text{pt}\right\} )$ as an open subspace of $X_n$ that admits a retraction
     \[
         X_{n-1} \cup (J \times (\mathring{D}^n \setminus \left\{\text{pt}\right\} )) \stackrel{r}{\longrightarrow}   X_{n-1}
    \] 
    Let $U', V'$ be separating, open neighborhoods of  $x,y\in X_{n-1}$. Then set $U\coloneqq  r^{-1}(U')$, $V \coloneqq r^{-1}(V')$ as separating open neighborhoods of $x,y\in X_n$.

    \begin{recap}
If $(X,A)$ is an NDR-pair, then
\[
    h\colon X \times I \to  X, \qquad h_0 = \id, h_1(u^{-1}([0,1))) = A
\]
$V\coloneqq u^{-1}([0,1)) \subset X$ is open and $h_1 \colon  V \to  A$, so that we have $h_1(a) = a$ for all $a\in A$. Thus, $h_1$ is a retraction.
    \end{recap}

    We now want to show that $X$ is Hausdorff.
    Let  $x,y\in X$ be given.
    Then there exists $n \geq -1$ such that $x,y\in X_n$.

    By \autoref{prop:relative-cw-is-gives-ndr-pair},  $(X,X_n)$ is an NDR-pair
     \begin{recap}
         If $(X,A)$ is a relative CW-complex, so is  $(X,X_n)$.
    \end{recap}
    By the above, there exists an open neighborhood $W\subset X$ of $X_n$ and a retraction  $r\colon  W \to  X_n$.
    Since $X_n$ is Hausdorff, so there exist separating open neighborhoods $U', V'\subset X_n$  of $x,y$.
    Then  $r^{-1}(U')$, $r^{-1}(V')$ are separating open neighborhoods of $x,y\in X$.
\end{proof}

\begin{definition}\label{def:cellular-map-of-relative-cw-complexes}
    A map $f\colon (X,A) \to  (Y,B)$ of relative CW-complexes is called \vocab{cellular}, if $f(X_n) \subset Y_n$ for $n\geq -1$.

    An \vocab{isomorphism} of relative CW-complexes is an isomorphism in the category of CW-complexes and cellular maps, i.e. a cellular map
    \[
        f\colon  (X,A) \to  (Y,B)
    \]
    such that there exists a cellular map $g\colon  (Y,B) \to  (X,A)$ with $g \circ  f = \id_{(X,A)}$ and $f \circ  g = \id_{(Y,B)}$.

    In particular, $f$ is a homeomorphism.
\end{definition}

\begin{example*}
    The map
    \missingfigure{Map of $I$ to  $I$ with different  CW-complex-structures}
    is a cellular homeomorphism, but the inverse is not cellular.
    So this is not an isomorphism of CW-complexes.
\end{example*}

\begin{dfact}
If $X$ and  $Y$ are isomorphic  CW-complexes, then $X_n\setminus X_{n-1} \cong Y_n\setminus Y_{n-1}$.
Thus $X$ and  $Y$ have the same number of  $n$-cells. 
\end{dfact}

\begin{theorem}[Cellular approximation]\label{thm:cellular-approximation}
    Let $f\colon (X,A) \to  (Y,B)$ a map of relative CW-complexes (sending $A$ to  $B$).
    Then  $f$ is homotopic relative  $A$ to a cellular map.
\end{theorem}

\begin{proof}
   We first show that if $\frestriction{f}{X_{m-1}}$  is cellular, then $\frestriction{f}{X_m}$ is homotopic to a cellular map relative to $X_{m-1}$.

   Since we want to fix $X_{m-1}$, we can define the homotopy on one cell of a time.
   Since $D^m$ is compact, $f(D^m)$ only intersects finitely many cells of  $Y$ (by exercise 2, sheet 6).
   Thus it suffices to show the following:

    \begin{claim}
       Let $m<n$,  $Y \coloneqq  B\cup _{\partial D^{n}} D^n$.
       Every mp $(D^m ,\partial D^m) \stackrel{g}{\longrightarrow} (Y,B)$ is homotopic to a map with image in $B$.
       \todo{don't we need this relative to $\partial D^m$?}
   \end{claim}

   \begin{oral}
       To be precise, we first note that we can restrict $X$ to have only one cell, since we can give the homotopy on each cell separately - in the end we just glue these together along the (fixed !) boundaries.

       Then, as  $f(D^m)$ intersects finitely many cells in $Y$, we can homotope $f$ off the corresponding cells one by one.
       Thus $X$ and  $Y$ have the same number of  $n$-cells. 
   \end{oral}

   We want to prove the claim by induction on $n$.
   The case  $n=1$ implies  $m=0$ and thus the claim stats the every map $\star \to  B \cup _{\partial D^1} D^1$ is homotopic to a map to $B$.
   This is true, since  $D^1$ is path connected.

   Now let $n\geq 2$ and assume the claim holds for $n-1$.
   We then get:

   \underline{Statement 1:} For $p<n-1$, every map  $h\colon  S^p \to S^{n-1}$  is nullhomotopic.
   \begin{proof}
       Write $S^{n-1} = \star \cup_{\partial D^{n-1}} D^{n-1}$ and $S^p = \star \cup_{\partial D^p} D^p$ with $h(\star) = \star$.
       Now apply our claim.
   \end{proof}

   \underline{Statement 2:} For $p<n-1$, every map  $f\colon  S^p \to  S^{n-1} \times (a,b)$ extends to a map $D^{p+1} \to  S^{n-1} \times (a,b)$.
   \begin{proof}
       $f_1\colon  S^p \to S^{n-1}$ is nullhomotopic and $(a,b)$ is contractible, so $f$ is nullhomotopic.
       A nullhomotopy of  $f$ is the same as an extension to $D^{p+1}$.
   \end{proof}
\end{proof}
