%! TEX root = ../../master.tex
\lecture[]{Di 21 Dez 2021}{Untitled}

\begin{oral}
We explain quickly what we mean by $f_*$ inducing  $f$ on  $H_0$ in
\autoref{lm:induced-chain-map-of-free-resolutions}.

First note that the homology of the chain complex
\[
\ldots \to  F_2 \to  F_1 \to  F_0 \to 0
\] 
is trivial (by exactness) except in degree $0$, where we have 
 \[
     H_0(F_*) \cong M
\]
Alternatively, one can consider the exact sequence
\[
\ldots \to  F_2 \to  F_1 \to  F_0 \to M \to  0
\] 
and take a chain map, that actually is given my $f\colon M \to  M'$ in the corresponding degree.
\end{oral}

\begin{proof}[Functoriality in \autoref{cor:ext-groups-are-well-defined-and-functorial}]
    Consider a map $M \stackrel{f}{\longrightarrow} M'$,
    we are interested in an induced map
    $\Ext_R^n(M, N) \to \Ext_R^n(M',N)$
    We use \autoref{lm:induced-chain-map-of-free-resolutions}
    to get an induced chain map
    \[
    \begin{tikzcd}
        F_1 \ar{r} \ar{d}{f_1}& F_0 \ar{r} \ar{d}{f_0}& M \ar{d}{f'} \\
        F_1' \ar{r} &  F_0' \ar{r} &  M'
    \end{tikzcd}
    \]
    This gives a map
    $\Hom(F_*', N) \stackrel{f^*}{\longrightarrow} \Hom(F_*, N)$
    so applying cohomology, this gives an induced map
    \[
        f^*\colon H^n(\Hom(F_*',N) \to H^n(\Hom(F_*, N))
    \] 
\end{proof}

\begin{refproof}{lm:induced-chain-map-of-free-resolutions}
    We construct $f_n$ inductively,
    for the start we can lift $f$ to $f_0$ since $F_0$ is free.

    \begin{tikzcd}
        F_0 \ar[dashed]{r}{f_0} \ar[two heads]{d}& F_0' \ar[two heads]{d} \\
        M \ar{r}{f} & M'
    \end{tikzcd}

    For the induction step, consider
    \[
    \begin{tikzcd}
        F_{n+1} \ar[swap]{d}{d_{n+1}} & F_{n+1}' \ar{d}{d_{n+1}'} \\
        F_n \ar{r}{f_n} \ar{d}[swap]{d_n} & F_n' \ar{d}{d_n'} \\
        F_{n-1} \ar{r}{f_{n-1}} & F_{n-1}'
    \end{tikzcd}
    \]
    Since $d_n \circ d_{n+1} = 0$, we have
    $d_n' \circ f_n \circ d_{n+1} = f_{n-1} \circ d_n \circ d_{n+1} = 0$.
    In particular, by exactness of $F'_*$ this gives that
    $f_n \circ d_{n+1}\colon F_{n+1} \to \ker d_n' = \im d_{n+1}'$.
    Since $F_{n+1}$ is free, a lift $f_{n+1}$ exists.

    Now let $f_*$ and $f_*'$ be two such lifts.
    Then the difference lifts the following diagram:
    \[
    \begin{tikzcd}
        F_1 \ar{r}{f_1 - f_1'} \ar{d}& F_1' \ar{d}\\
        F_0 \ar{r}{f_0 - f_0'} \ar{d}& F_0'\ar{d} \\
        M \ar{r}{0} & M'
    \end{tikzcd}
    \]
    We want to construct a chain homotopy $h_*$ inductively.
    By exactness of $F_*'$, and since  $F_0$ is free, $h_0$ exists.

    Assume we defined $h_{n-2}$ and $h_{n-1}$ such that
    $f_{n-1} - f_{n-1}' = d_n' \circ h_{n-1} + h_{n-2} \circ d_{n-1}$.
\[
    \begin{tikzcd}[sep = large]
        F_{n+1} \ar{r} \ar{d}[swap]{d_{n+1}} & F_{n+1}' \ar{d}{d_{n+1}'} \\
        F_n \ar{r}{f_n - f_n'} \ar[swap]{d}{d_n} & F_n' \ar{d}{d_n'} \\
        F_{n-1} \ar[swap]{d}{d_{n-1}} \ar{r}{f_{n-1} - f_{n-1}'} \ar{ur}{h_{n-1}}& F_{n-1}' \ar{d}{d_{n-1}'} \\
        F_{n-2} \ar{r} \ar{ur}{h_{n-1}}& F_{n-2}'
    \end{tikzcd}
    \]
    Then
    \begin{IEEEeqnarray*}{rCl}
        d_n' \circ ((f_n - f_n') - h_{n-1} \circ d_n)
        & = & (f_{n-1} - f_{n-1}') \circ d_n - d_n' \circ h_{n-1} \circ d_n \\
        & = & h_{n-2} \circ d_{n-1} \circ d_n \\
        & = & h_{n-2} \circ 0 = 0
    \end{IEEEeqnarray*}

    Hence (again by exactness and freeness),
    there is a lift $h_n \colon F_n \to F_{n+1}'$
    of $(f_n - f_n') - (h_{n-1} \circ d_n)$.
    But this just means that $d_{n+1}' \circ h_n = (f_n - f_n') - (h_{n+1} \circ d_n)$,
    or just $f_n - f_n' = d_{n+1}' \circ h_n - h_{n-1} \circ d_n$.
\end{refproof}

\begin{oral}
    Just note that we actually just used
    exactness of $F'_*$ and freeness of  $F_*$
    in the proof of the lemma.
\end{oral}

\begin{proposition}\label{prop:long-exact-sequence-of-ext-groups}
    Let $0 \to A \to  B \to  C \to  0$ be a short exact sequence of $R$-modules,
    and let  $M$ be an  $R$-module.
    Then, we have a natural long exact sequence
\[
    \begin{tikzcd}[column sep = tiny]
        0 \ar{r} & \Hom_R(C,M) \ar{r} & \Hom_R(B,M) \ar{r} \ar[phantom, d, ""{coordinate, name=Z1}]& \Hom_R(A,M)
        \ar[swap,dll,
        "δ",
        rounded corners,
        to path = {
            -- ([xshift=2em]\tikztostart.east)
            |- (Z1) [near end]\tikztonodes
            -| ([xshift=-2ex]\tikztotarget.west)
        -- (\tikztotarget)}] \\
                 & \Ext^1_R(C,M) \ar{r} & \Ext^1_R(B,M) \ar{r} \ar[phantom, d, ""{coordinate, name=Z2}]& \Ext^1_R(A,M)
        \ar[swap,dll,
        "δ",
        rounded corners,
        to path = {
            -- ([xshift=2em]\tikztostart.east)
            |- (Z2) [near end]\tikztonodes
            -| ([xshift=-2ex]\tikztotarget.west)
        -- (\tikztotarget)}] \\
                 & \Ext^2_R(C,M) \ar{r} & \Ext^2_R(B,M) \ar{r} & \ldots \\
    \end{tikzcd}
\]
\end{proposition}
\begin{proof}
     Let $F_*^A$ and  $F_*^C$ be free resolutions of  $A$ and $C$ respectively.
     We wont to construct a free resolution for $B$ such that we get an exact sequence
      \[
        0 \to  F_A \to  F_B \to F_C \to 0
     \] 
     So consider
     \[
     \begin{tikzcd}
         &F_0^A \ar{r} \ar[swap]{d}{d_0^A}&
         F_0^A \oplus F_0^C \ar{r} \ar[dotted]{d}{d_0^B} &
         F_0^C \ar{d}{d_0^C}\ar[dashed]{dl}{s} \\
         0 \ar{r} &  A \ar[swap]{r}{f} & B \ar[swap]{r}{g} & C \ar[two heads]{r} & 0
     \end{tikzcd}
     \]
     and define
     \[
         d_0^B(x,x') = f(d_0^A(x)) + s(x')
     \]
     We claim that this is surjective.
     For $b\in B$ let $x' \in F_0^C$ be a preimage of $g(b)$.
     Then $g(b - s(x')) =0$.
     Hence  $b-s(x')\in \ker g = \im f$,
     and by viewing $A\subset B$ via $f$ we get that
     $b-s(x')\in \im d_0^A$.
     So we can pick $y\in F_0^A \stackrel{d_0^A}{\longrightarrow}  b-s(x')$.
     Then $d_0^B(y, x') = b$ by definition so that $d_0^B$ is surjective.

     By exercise 3.2, the induced sequence
     \[
     0 \to  \ker d_0^A \to \ker d_0^B \to  \ker d_0^C \to 0
     \] 
     is exact.

     Inductively, we thus get a free resolution $F_*^B$ of  $B$ with
     $F_n^B \cong F_n^A \oplus F_n^C$ such that
      \[
     0 \to F_*^A \to F_*^B \to F_*^C \to 0
     \] 
     is exact.

     Hence also
     \[
         0 \to \Hom_R(F_*^C,M) \to \Hom_R(F_*^B,M) \to \Hom_R(F_*^A,M) \to 0
     \] 
     is exact.

     Thus we get an induced LES in cohomology\todo{ref},
     using \autoref{rm:ext-0-is-just-hom}.
\end{proof}

\begin{oral}
    Note that it is not sufficient to just set $F_n^B = F_n^A + F_n^C$ with the
    corresponding maps directly.
\end{oral}

\begin{dremark}\label{rm:ext-0-is-just-hom}
    Note that by left-exactness of $\Hom_R(-,M)$,
    for $R$-modules $X,M$ we get in general
    \[
        \Ext^0_R(X,M) = H^0(\Hom_R(F_*^X, M)) = \ker(\Hom_R(F_0^*,M) \to \Hom_R(F_1^*,M))
    \]
    which is isomorphic to
    \[
        \Hom_R(X,M)
    \] 
\end{dremark}

\begin{theorem}\label{thm:ses-ext-groups-and-kronecker-pairing}
    Let $R$ be a principal ideal domain,
    $C_*$ a free  $R$-cochain complex and $M$ an $R$-module. 
    Let $H^n(C;M)$ denote the  $n$-th  cohomology of  $\Hom(C_*;M)$. 

    Then there is a natural (in $C_*$ and  $M$) short exact sequence
     \[
         0 \to \Ext^1_R(H_{n-1}(C_*), M)
         \to  H^n(C,M)
         \stackrel{\left< -,- \right> }{\longrightarrow}  \Hom(H_n(C_*), M)
         \to 0 
    \]
\end{theorem}
\begin{dremark}
    Note that this SES splits by
    \autoref{lm:curried-kronecker-pairing-is-split-surjective},
    but this splitting is not natural.
\end{dremark}
\begin{proof}
    Let $B_{n-1}\subset C_{n-1}$ be the image of $d_n$ and  $Z_n \subset C_n$ the kernel.
    Since $R$ is a PID,  $B_{n-1}$ and $Z_n$ are free.
    Applying $\Hom_R(-, M)$ to
    \[
    0 \to Z_n \to C_n \to B_{n-1} \to 0
    \]
    yields a short exact sequence
    \[
        0 \to \Hom_R(B_{n-1},M) \to \Hom_R(C_n,M) \to \Hom_R(Z_n,M) \to 0
    \]

    We can view this as a SES of cochain complexes
    \[
        0 \to \Hom_R(B_{*-1},M) \to \Hom_R(C_*, M) \to \Hom_R(Z_*,M) \to 0
    \]
    using the trivial differential for $B_{*-1}$ and $Z_*$, as
    \[
    \begin{tikzcd}
        Z_n \ar[hook]{r} \ar[swap]{d}{0} &
        C_n \ar{d}{d_n} \ar{r}{d_n} & B_{n-1} \ar{d}{0} \\
        Z_{n-1} \ar[hook]{r} &C_{n-1} \ar{r}{d_{n-1}} & B_{n-2}
    \end{tikzcd}
    \]
    commutes.
    Then we get a LES in cohomology
    \[
        \ldots \to  \Hom_R(Z_{n-1}, M)
        \stackrel{δ}{\longrightarrow} \Hom_R(B_{n-1},M)
        \to H^n(C,M)
        \to \Hom_R(Z_n,M) \stackrel{δ}{\longrightarrow} \ldots
        \tag{$\star$}
    \]
    \begin{claim}
        The connecting homomorphisms $δ$ are induced by the inclusion $B_n \subset Z_n$.
    \end{claim}
    \begin{proof}
        Let $s\colon C_n \to Z_n$ be a splitting of $Z_n \to C_n$.
        Then $δ(f\colon Z_n \to M)$ is given by
        \[
        \begin{tikzcd}
            Z_{n+1} \ar{r} & C_{n+1} \ar{d}{d_n}\ar{r}{d_{n+1}} & C_n \ar{r}{s} & Z_n \ar{r}{f} & M \\
                           & B_n \ar[swap]{urrr}{δf} \ar[hook, red]{ur} \ar[hook, red]{urr}
        \end{tikzcd}
        \]
        so that $δf = f \circ i_n$.

        From the SES
        \[
            0 \to B_n \stackrel{i_n}{\longrightarrow} Z_n \to H_n(C) \to 0
        \]
        we get a LES in the $\Ext$ groups by
        \autoref{prop:long-exact-sequence-of-ext-groups}
        \[
        \begin{tikzcd}
            0 \ar{r} & \Hom_R(H_n(C_*),M) \ar{r} & \Hom_R(Z_n,M) \ar{r}{i_n^*} &  \Hom_R(B_n,M) \\
            \ar{r} & \Ext^1_R(H_n(C_*),M) \ar{r} & \Ext^1_R(Z_n,M) =0
        \end{tikzcd}
        \tag{$\star \star$}
        \]
        since $Z_n$ is free. 
        
        Now $(\star)$ gives a SES
         \[
             0 \to  \coker \Hom_R(i_{n-1},M) \to H^n(C,M) \to \ker \Hom_R(i_n,M) \to 0
        \] 
        Using $(\star\star)$ we thus get
         \[
             0 \to \Ext_R^1(H_{n-1}(C_*),M) \to H^n(C,M) \to \Hom_R(H_n(C_*),M) \to 0
        \] 
        It remains to show that the last map really is the Kronecker pairing $\left< -, - \right> $.
        But $H^n(C,M) \to \Hom_R(Z_n,M)$ in $(\star)$ is given by sending
        $[f\colon C_n\to M]$ to its restriction to $Z_n$.
        The identification of  $\Hom_R(H_n(C),M)$ with  $\ker(i_n,M)$ sends it to
            \begin{equation*}
            \begin{array}{c c l} 
                H_n(C) & \longrightarrow & M \\
                \left[ x \right]  & \longmapsto &  f(x)
            \end{array}
        \end{equation*}
        which is just $\left< \left[ f \right] , - \right> $
    \end{proof}
\end{proof}
\begin{recap}
    If $f\colon Z_n \to M$ is in the kernel of $i_n^*$,
    i.e\ the composition
    $B_n \xrightarrow{i_n} Z_n \xrightarrow{f} M$
    is trivial,
    then we get an induced map
    \begin{tikzcd}
        B_n \ar{r} & Z_n \ar{d}\ar{r}{f} & M \\
                   & H_n(C_*) \ar[dashed,swap]{ur}{f}
    \end{tikzcd}
\end{recap}

\begin{corollary}[Universal coefficient theorem]\label{cor:universal-coefficient-theorem}
    Let $R$ be a PID.
    There is a natural SES
    \[
        0
        \to  \Ext^1_RR(H_{n-1}(X;R);M)
        \to H^n(X;M)
        \xrightarrow{\left< - , - \right> } \Hom_R(H_n(X;R), M)
        \to 0
    \] 
\end{corollary}
\begin{proof}
    Apply \autoref{thm:ses-ext-groups-and-kronecker-pairing}
    to $C_*(X;R)$.
\end{proof}

\begin{lemma}\label{lm:ext-groups-of-z-modules}
    Let $A$ be a  $\mathbb{Z}$-module.
    \begin{enumerate}[h]
        \item $\Ext^1_{\mathbb{Z}}\left( \faktor{\mathbb{Z}}{n}, A \right) \cong \faktor{A}{nA}$
        \item $\Ext^1_{\mathbb{Z}}(\mathbb{Z},A) = 0$ 
        \item $\Ext^1_\mathbb{Z} (B \oplus B', A)
            \cong \Ext^1_{\mathbb{Z}} (B , A) \oplus \Ext^1_{\mathbb{Z}} (B', A)$.
    \end{enumerate}
\end{lemma}
\begin{proof}
    \begin{enumerate}[1), wide = 0pt]
        \item 
    Consider the free resolution
    \[
    0
    \to  \mathbb{Z}
    \xrightarrow{\cdot n} \faktor{\mathbb{Z}}{n\mathbb{Z}}
    \to 0
    \]
    and thus
    \begin{IEEEeqnarray*}{rCl}
        \Ext^1_{\mathbb{Z}} \left( \faktor{\mathbb{Z}}{n}, A \right) 
        &\cong&
        \coker(
        \Hom_{\mathbb{Z}} (\mathbb{Z} , A)
        \xrightarrow{\cdot n} \Hom_{\mathbb{Z}} (\mathbb{Z} , A)
        )
        \\
        &\cong&
        \coker (A \xrightarrow{\cdot n} A)
        \\
        &\cong&
        \faktor{A}{nA}
        .
    \end{IEEEeqnarray*}

    \item A free resolution is just
        \[
            0 \to \mathbb{Z} \xrightarrow{{\id}}  \mathbb{Z} \to 0
        \] 
        and thus
        \[
            \Ext_{\mathbb{Z}}^1 (\mathbb{Z} , -) = 0
        .\] 
    \item If $F_*^B$,  $F_*^{B'}$ is a free resolution of $B \oplus B'$,
         $F_*^B \oplus F_*^{B'}$ is a free resolution of $B \oplus B'$.
         We deduce that
          \[
              \Hom_{\mathbb{Z}}( F_n \oplus F_n' , A)
              \cong
              \Hom_{\mathbb{Z}} ( F_n , A ) \oplus \Hom_{\mathbb{Z}} ( F_n' , A )
         \] 
    \end{enumerate}
\end{proof}

\begin{dabuse}
        Sometimes we will just write
        \[
            \Ext_R (M, N) \coloneqq \Ext^1_R (M , N)
        \]
        This is especially the case when working over $PID$s,
        since in this case,
        higher $\Ext$ groups vanish anyway.
\end{dabuse}

\begin{corollary}[Cohomology of real projective space]
\label{cor:cohomology-of-real-projective-space}
    We have
    \[
        H^n(\mathbb{R}\mathbb{P}^k ; \mathbb{Z})
        \cong
        \begin{cases}
            \mathbb{Z} & \text{$n = 0$ or $k=n$ odd} \\
            \faktor{\mathbb{Z}}{2} & \text{$n\leq k$ even, $n\neq 0$} \\
            0 & \text{else}
        \end{cases}
    \] 
\end{corollary}

\begin{proof}
    Using the \nameref{cor:universal-coefficient-theorem},
    we get the exact sequence
    \[
    0
    \to
    \Ext^1_{\mathbb{Z}} ( H_{n-1} ( \mathbb{R}\mathbb{P}^k ; \mathbb{Z}) , \mathbb{Z})
    \to
    H^n(\mathbb{R}\mathbb{P}^k ; \mathbb{Z})
    \to
    \Hom_{\mathbb{Z}} ( H_n(\mathbb{R}\mathbb{P}^k ; \mathbb{Z}) ; \mathbb{Z})
    \to
    0
    \]
    But using \autoref{lm:ext-groups-of-z-modules},
    we can deduce
    \[
        \Ext^1_{\mathbb{Z}} ( H_{n-1}(\mathbb{R}\mathbb{P}^k ; \mathbb{Z} ) , \mathbb{Z} )
        \cong
        \begin{cases}
            \faktor{\mathbb{Z}}{2} & \text{$n-1\leq k$ odd} \\
            0 & \text{else}
        \end{cases}
    .\]
    Now with the homology of the sphere one gets
    \[
        \Hom_{\mathbb{Z}}( H_n(\mathbb{R}\mathbb{P}^k ; \mathbb{Z}) , \mathbb{Z})
        \cong
        \begin{cases}
            \mathbb{Z} & \text{$n = 0$,  $k = n$ odd} \\
            0 & \text{else}
        \end{cases}
    \] 
    and thus our result follows as the SES splits.
\end{proof}

\begin{example}
    The splitting from
    \autoref{lm:curried-kronecker-pairing-is-split-surjective}
    can't be natural.
    
    \todo{Finish this eample}
\end{example}
