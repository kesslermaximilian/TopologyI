%! TEX root = ../../master.tex
\lecture[Homology of $\mathbb{R}\mathbb{P}^2$. Pushouts. (strong) (deformation) Retractions, NDR pairs. Pushouts give NDR pairs and $H_*$-isomorphism. Triples of spaces and their long exact sequences.]{Mi 27 Okt 2021}{NDR pairs and their properties}

\begin{orga}
    As the next Monday is holiday, there will be no Q\&A session.
\end{orga}

\begin{example*}\label{ex:mayer-vietoris-sequence-for-real-projective-plane}
Take a homology theory and assume that $H$ satisfies the dimension axiom. Consider $\mathbb{R}\mathbb{P}^2$, and take $V,U$ as in the bottom figure to establish an excisive triad. Then  $V \simeq \star$ and  $U \simeq S^1$. Also  $U\cap V \simeq S^1$.

    \missingfigure{$\mathbb{R}\mathbb{P}^2$ as excisive triad}

    Also note that the inclusion is given by
        \begin{equation*}
        \begin{array}{c c l} 
        \underbrace{U\cap V}_{\simeq S^1} & \longrightarrow & \underbrace{U}_{\simeq S^1} \\
        z & \longmapsto &  z^2
        \end{array}
    \end{equation*}
    Thus, by the \nameref{thm:mayer-vietoris}, we get the exact sequence
    \[
    \begin{tikzcd}
        \underbrace{H_2(S^1) \oplus H_2(\star)}_{=0} \ar{r}&  H_2(\mathbb{R}\mathbb{P}^2) \ar{r}{} &  H_1(S^1) \ar{r} & H_1(S^1) \oplus H_1(\star) \ar[]{dlll}  \\
        H_1(\mathbb{R}\mathbb{P}^2) \ar{r}{\partial} &  H_0(S^1) \ar{r} & H_0(S^1) \oplus H_0(\star) \ar{r} & H_0(\mathbb{R}\mathbb{P}^2) \ar{r} & 0
    \end{tikzcd}
\]
This yields the exact sequence
\[
\begin{tikzcd}
    0 \ar{r} & H_2(\mathbb{R}\mathbb{P}^2) \ar{r} & \mathbb{Z} \ar{r}{\cdot 2} &  \mathbb{Z} \oplus 0 \ar{r}{p} &  H_1(\mathbb{R}\mathbb{P}^2) \ar{r} & \mathbb{Z} \ar{r}{\Delta} &  \mathbb{Z}\oplus \mathbb{Z} \ar{r}{} &  H_0(\mathbb{R}\mathbb{P}^2) \ar{r} & 0
\end{tikzcd}
\]

Thus by exactness, we see that $H_2(Rr\mathbb{P}^2) = 0$ and
\[
    H_1(\mathbb{R}\mathbb{P}^2 \cong \im p \cong \coker(\cdot 2) = \faktor{\mathbb{Z}}{2\mathbb{Z}}
\] 
as well as
\[
    H_0(\mathbb{R}\mathbb{P}^2) \cong \coker \Delta \cong \faktor{\mathbb{Z}\oplus \mathbb{Z}}{\im \Delta} \cong \mathbb{Z}
\] 
Thus,
\[
    H_n(\mathbb{R}\mathbb{P}^2) \cong \begin{cases}
        \mathbb{Z} & n = 0 \\
        \faktor{\mathbb{Z}}{2\mathbb{Z}} & n = 1 \\
        0 & n\geq 2
    \end{cases}
\] 
\end{example*}

\begin{goal}
    We now want to sort of generalize our result (\nameref{thm:mayer-vietoris}), by first giving an additional class of excisive triads, as well as a generalization with an additional subspace in the statement of the theorem.
\end{goal}

\def\pushoutsymbol{\tikz[baseline=0.5,scale=0.2]{
        \draw[-] (0,0) --(0,1) -- (1,1);
    \draw (1,0) circle (1.5pt);}
}

\begin{definition}[Pushout]\label{def:pushout}
    A diagram (in a category $\mathcat{C}$) of the form
    \[
    \begin{tikzcd}
        X_0 \pushout\ar[swap]{d}{i_2} \ar{r}{i_1} & X_1 \ar{d}{j_1} \\
        X_2 \ar[swap]{r}{j_2} & X
    \end{tikzcd}
    \]
    if it has the universal property that for all $Y\in \mathcat{C}$ and all $f_k\colon X_k \to Y$ with $f_1 \circ  i_1 = f_0 = f_2 \circ i_2$, there exists a unique map $f\colon X\to Y$ such that $f \circ  j_k = f_k$ for $k=1,2$.
    \[
    \begin{tikzcd}
        X_0 \pushout\ar[swap]{d}{i_2} \ar{r}{i_1} & X_1 \ar{d}{j_1} \ar[bend left=20]{ddr}{f_1}\\
        X_2 \ar[bend right=20, swap]{drr}{f_2}\ar[swap]{r}{j_2} & X \ar["\exists !" description, red]{dr} \\
                              & & Y
    \end{tikzcd}
    \]
\end{definition}

\begin{remark*}
    In spaces, for every diagram
    \[
    \begin{tikzcd}
        X_0 \ar[swap]{d}{i_2}\ar{r}{i_1} &X_1 \\
        X_2
    \end{tikzcd}
\]
there exists a pushout that is given by
\[
    X_1\bigcup_{X_0} X_2 \coloneqq  \faktor{X_1 \coprod X_2}{\substack{i_1(x) \sim i_2(x)\\\forall x\in X_0}}
\] 
together with the canonical map $X_i \to X_1 \cup_{X_0}X_2 $
\end{remark*}

\begin{definition}[Retractions]\label{def:retraction-deformation-retract-ndr-pair}
    Let $i\colon A \to X$ be the inclusion of a subspace.
    \begin{enumerate}
        \item A \vocab{retraction} is a map $r\colon X \to A$ such that $r \circ  i = \id_A$.
\item $r$ is called a \vocab{deformation retract} if $i \circ r \simeq \id_X$
\item and a \vocab{strong deformation retract}, if $i \circ  r \simeq \id_X$ relative to $A$. 
    \end{enumerate}

    It $r$ exists,  $A$ is called restract or (strong) deformation retract respectively.

    \begin{enumerate}[resume]
        \item $(X,A)$ is a  \vocab{neighborhood deformation retract} (abbreviated NDR), if there exists a map $u\colon X \to  [0,1]$ with $A = u^{-1}(0)$ , a homotopy
            \[
                H\colon X \times [0,1] \to  X
            \] 
            such that $H_0 = \id_X$, $H_t(a) = a \; \forall a\in A, t\in [0,1]$ and $H_1(x) \in A\forall x\in u^{-1}([0,1))$
    \end{enumerate}
\end{definition}

\begin{remark}
    If $(X,A)$ is an NDR pair, then $A\subset X$ is closed since $A = u^{-1}(0)$.
\end{remark}

\begin{example*}
    We will soon see that $(D^n, S^{n-1})$ is not a retract, but an NDR-pair.
\end{example*}

\begin{editor}
    In the version presented in lecture 5, \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad}  said that $(X,X_1,X_2)$ is an excisive triad. This is not the case, because $X_1$ is not necessarily a subspace of $X$.

    Instead, we will develop the following:
\end{editor}

\begin{definition*}\label{def:h-star-isomorphism}
    A map $f\colon (X,A) \to (Y,B)$ is an $H_*$-isomorphism if  $f_*\colon H_n(X,A) \to H_n(Y,B)$ is an isomorphism.
\end{definition*}

\begin{example*}
    If $(X,X_1,X_2)$ is an excisive triad, then $(X_1,X_1\cap X_2) \to (X,X_2)$ is an $H_*$-isomorphism by definition.
\end{example*}

\begin{proposition}\label{prop:pushout-for-ndr-pair-gives-excisive-triad}
    Consider a pushout
    \[
    \begin{tikzcd}
        X_0 \ar[swap]{d}{i_2}\pushout \ar{r}{i_1} & X_1 \ar{d}{j_1} \\
        X_2 \ar[swap]{r}{j_2} & X
    \end{tikzcd}
    \]
    such that $(X_1,X_0)$ is an NDR-pair, then $(X,X_2)$ is an NDR-pair and $(X_1,X_0) \to  (X,X_2)$ is an $H_*$-isomorphism.
\end{proposition}

\begin{oral}
    Taking $X_0 = S^{n-1}$ and $X_1 = D^n$ 'attaches' a cell to $X_2$. When we see CW-complexes in a few weeks, \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad} and \autoref{thm:mayer-vietoris} will help us compute the homology of these CW-complexes, as these are constructed by successively attaching cells to a space.
\end{oral}

\begin{corollary}\label{cor:delta-complex-pushout}
    Let $X$ be a  $\Delta$-complex, and set
     \[
         X_n \coloneqq \left \{\parbox{12em}{\centering All points in the image of some $α\in \bigcup_{i=0} ^nA_n$}\right\} 
    \]
    Then $(X_n, X_{n-1})$ is an NDR-pair.
    Actually,
    \[
    \begin{tikzcd}
        \coprod_{A_n} \pushout\partial\Delta^n \ar[swap]{d}{\partialα_n} \ar{r}{} & \coprod_{A_n}\Delta^n \ar{d}{} \\
        X_{n-1} \ar[swap]{r}{j} & X_n
    \end{tikzcd}
    \]
    is a pushout and $\left( \coprod_{A_n}\Delta^n, \coprod_{A_n}\partial\Delta^n \right) $ is a NDR pair.
\end{corollary}

\begin{proof}
   \begin{enumerate}[1.]
       \item First we show that the diagram is in fact a pushout. Since $α_n \circ \delta^i\in A_{n-1}$, $\coprod \partial α_n$ has image in $X_{n-1}$. Now let $Y$ be the pushout

 \[
 \begin{tikzcd}
     \coprod \partial\Delta^n \ar[swap]{d}{} \ar{r}{} \pushout& \coprod_{A_n}\Delta^n \ar{d}{p_1}\ar[bend left = 20]{ddr} \\
     X_{n-1} \ar[swap]{r}{p_2} \ar[bend right = 20]{drr}& Y \ar["f" description, dashed]{dr}\\
                               & & X_n
 \end{tikzcd}
 \]
 We know that
 \[
     Y = \faktor{X_{n-1} \coprod \coprod\limits_{A_n}\Delta^n}{\sim }
 \] 
 As sets this is $X_{n-1}\coprod \coprod\limits_{A_n}\mathring{\Delta}^n$. Thus, $f$ is a bijection because very point in  $X_n$ is in the interior of precisely one simplex. We also know that $U\subset Y$ is open if $p_1^{-1}(U), p_2^{-1}(U)$ are open by the property of the quotient topology. $U\subset X_n$ is open iff $α^{-1}(U)$ is open for all $α\in \bigcup_{i=0} ^n A_i$, which is the case iff
 \[
     j^{-1}(U)\subset X_{n-1}
 \] 
 is open and $\coprod_{A_n}α^{-1}(U)$ is open in $\coprod_{A_n}\Delta^n$.

 So we conclude that $U\subset Y$ is open iff $f(U)\subset X_n$ is open. Thus, $f$ is a homeomorphism and  $X_1$ is a pushout.
 \item It suffices to show this for a single pair $(\Delta^n, \partial\Delta^n)\cong (D^n, S^{n-1})$, since we can then take the disjoint union of the constructed homotopy maps.

     \missingfigure{Parametrization of $D^n$}

     We define
         \begin{equation*}
         u: \left| \begin{array}{c c l} 
         D^n & \longrightarrow & I \\
         x & \longmapsto &  \min \left \{2-2\norm{x} ,1\right\} 
         \end{array} \right.
     \end{equation*}

     Then setting
         \begin{equation*}
         h: \left| \begin{array}{c c l} 
         D^n\times I & \longrightarrow & D^n \\
         (x,t) & \longmapsto &  \left( \min \left \{\norm{x} + t\norm{x} ,1\right\}  \right) \cdot \frac{x}{\norm{x}}
         \end{array} \right.
     \end{equation*}
     We verify that $u^{-1}(0) = S^{n-1}$, and also
     \[
         h(x,0) = \norm{x} \cdot \frac{x}{\norm{x}} = x \qquad \implies h_0 = \id_X
     \] 
     \[
\qquad \forall x\in S^{n-1}\colon          h(x,t) = 1\cdot \frac{x}{\norm{x} } = x
     \] 
     \[
         h(x,1) = \underbrace{\min \left \{2\norm{x} ,1\right\}}_{=1 \forall x\colon  \norm{x} \geq \frac{1}{2}}
     \] 
     but if $x\in u^{-1}([0,1))$, then $2-2\norm{x}<1 $ and thus $\frac{1}{2}<\norm{x}$ and thus $h(x,1) = \frac{x}{\norm{x} }\in S^{n-1}$ as required.
   \end{enumerate} 
\end{proof}

\begin{lemma}\label{lm:pushout-ist-stable-under-product-with-unit-interval}
    If the diagram
    \[
    \begin{tikzcd}
        X_0 \ar[swap]{d}{i_2} \ar{r}{i_1} & X_1 \ar{d}{} \\
        X_2 \ar[swap]{r}{} & X
    \end{tikzcd}
    \]
    is a pushout, then so is
    \[
    \begin{tikzcd}
        X_0\times I \ar[swap]{d}{i_2\times \id} \ar{r}{i_1\times \id} & X_1\times I \ar{d}{} \\
        X_2\times I \ar[swap]{r}{} & X\times I
    \end{tikzcd}
    \]
    
\end{lemma}

\begin{proof}
We know that
\[
    X \cong \faktor{X_1 \coprod X_2}{i_1(x) \sim i_2(x)}
\] 
Last term we showed that if $A\stackrel{p}{\longrightarrow} B$ is a quotient map, so is $A\times I \stackrel{p\times \id}{\longrightarrow} B\times I$.
\todo{Reference on corresponding exercise?}
Thus we conclude that
\[
    X\times I \cong \faktor{X_1\times I \coprod X_2\times I}{(i_1(x),t)\sim (i_2(x),t)}
\] 
so $X\times I$ is a pushout of the second diagram.
\end{proof}

\begin{trivial*}
    One can also show \autoref{lm:pushout-ist-stable-under-product-with-unit-interval} with a bit more category theory. First one checks that taking the cross product with $I$ is adjoint to taking maps from the interval to the space. Using the fact that a right adjoints commute with colimits, one deduces the above result.
\end{trivial*}

\begin{refproof}{prop:pushout-for-ndr-pair-gives-excisive-triad}
    Let $(X_1,X_0)$ be an NDR pair. Let $u\colon X_1 \to I$ and $h\colon X_1 \times I \to X_1$ be as in the definition of a NDR pair. Then consider the following diagrams:
    \[
    \begin{tikzcd}
        X_0 \ar[swap, hook]{d}{} \ar{r}{} & X_1 \ar{d}{j_1}\ar[bend left = 20]{ddr}{u} \\
        X_2 \ar[bend right = 20,swap, hook]{drr}{c_0}\ar[swap]{r}{j_2} & X\ar["u'" description, dashed, blue]{dr} \\
                                                        & & I
    \end{tikzcd}
    \qquad
    \begin{tikzcd}
        X_0\times I \ar[swap]{d}{} \ar{r}{} & X_1\times I \ar{d}{}\ar[bend left = 20]{ddr}{j_1\circ h} \\
        X_2\times I \ar[swap]{r}{} \ar[bend right = 20, swap]{drr}{j_1 \circ  \pr_1}& X\times I \ar[dashed, "h'" description, blue]{dr}\\
                                                                                    & & X
    \end{tikzcd}
    \]
    Then $u'^{-1}(0) = \underbrace{u^{-1}(0)}_{X_0}\cup_{X_0} X_2 = X_2$

    For the computation of $h_0'$ we get the commutative diagram
   \[
   \begin{tikzcd}
       X_0 \ar[swap]{d}{} \ar{r}{} & X_1 \ar{d}{}\ar[bend left=20]{ddr}{j_1\circ h_0 = j_1} \\
       X_2 \ar[swap]{r}{} \ar[bend right=20,swap]{drr}{j_2} & X \ar[dashed, "h_0'" description]{dr}\\
       & & X
   \end{tikzcd}
   \]
   so that $h_0' = \id$, as the map making this square commute is unique. For $h_1'$ we get that
   \[
       h_1'(u'^{-1}(0,1))) = h_1(u^{-1}([0,1))) \cup _{X_0} X_2 = X_0 \cup _{X_0} X_2 = X_2
   \] 
       \todo{did not get this}
       Thus, $(X,X_2)$ is an NDR pair.
       It remains to show that $(X,X_1,X_2)$ is indeed an excisive triad. Consider $V_0 \coloneqq u^{-1}([0,1))$ and $V_2 \coloneqq u'^{-1}([0,1))$.
       Then $X_0\hookrightarrow  V_0$ and $X_2\hookrightarrow V_2$ are homotopy equivalences.
       We want to proof that $H_n(X_1,X_0) \to H_n(X,X_2)$ is an isomorphism. We get:
       \[
       \begin{tikzcd}
           H_n(X_1,X_0) \ar[swap]{d}{\cong}[swap]{\autoref{lm:homotopy-equivalences-on-space-and-subspace-induce-isomorphisms} } \ar{r}{} & H_n(X,X_2) \ar{d}{\cong} \\
           H_n(X_1,V_0) \ar[swap]{r}{} & H_n(X,V_2) \\
           H_n(X_1\setminus X_0, V_0\setminus X_0) \ar{r} \ar{u}{\cong}& H_n(X\setminus X_1, V_2\setminus X_2) \ar{u}{\cong}
       \end{tikzcd}
       \]
       Where we get the bottom isomorphisms by the excision axiom.
       \begin{claim}
           The map
           \[
           \frestriction{j_1 }{X_1\setminus X_0}\colon X_1\setminus X_0\to X\setminus X_2
           \] 
           is a homeomorphism with $j_1(V_0\setminus X_0) = V_2\setminus X_2$.
       \end{claim}
       \begin{subproof}
          Consider the commutative square
          \[
          \begin{tikzcd}
              X_1\setminus X_0 \ar[swap]{d}{} \ar{r}{} & X_1\coprod X_2 \ar{d}{} \\
              X\setminus X_2 \ar[swap]{r}{} & \faktor{X_1\coprod X_2}{i_1(x) \sim i_2(x)} \cong X
          \end{tikzcd}
          \]
       \end{subproof}
       Removing $X_2$ gives that $j_1|$ is a bijection.
       It remains to show that $j_1|$ is open.
       So let $U\subset X_1\setminus X_0$ be open, then - as $X_0$ is closed - we have $U\subset X_1$ open and thus
       \[
           p^{-1} (p(U)) = U
       \] 
       since $U\cap X_0 = \emptyset$. It follows that $p(U)$ is open. Thus,
        \[
       X_1 \setminus X_0 \to  X\setminus X_2
       \] 
       is a homeomorphism. We also check that
       \[
           V_2 = u'^{-1}([0,1)) = \underbrace{j_1(u^{-1}([0,1)))}_{=V_0} \cup_{X_0} j_2(X_2)
       \] 
       so that
       \[
       V_2\setminus X_2 \cong V_0\setminus X_0
       \] 
\end{refproof}

\begin{oral}
    What we established with \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad} now has several nice consequences: We do not have to work with open sets to find excisive triads, such that we can also apply \nameref{thm:mayer-vietoris} to NDR pairs (and their pushouts). Especially when dealing with CW-complexes, or more generally when we glue two spaces along some subspace, in most situations we will find an NDR pair and thus use \nameref{thm:mayer-vietoris}. This makes proofs shorter, as we do not have to artificially thicken the glued area to obtain open subspaces.
\end{oral}

\begin{oral}
    We will now establish yet another (technical) tool that will help us compute homology in the future, namely triple sequences.
\end{oral}


\begin{ddefinition}
    A \vocab{triple of spaces} is a space $X$ with subspaces  $A\subset B\subset X$. 
\end{ddefinition}

\begin{lemma}\label{lm:long-exact-sequence-of-triple-of-spaces}
    Let $(x,B,A)$ be a triple of spaces. Then there is a natural long exact sequence
    \[
    \begin{tikzcd}
        \ar{r}{\partial} &  H_n(B,A)    \ar{r}{i_*} &  H_n(X,A) \ar{r}{j_*} &  H_n(X,B) \ar{r}{\partial} &  H_{n-1}(B,A)
    \end{tikzcd}
    \]
    where $i\colon (B,A) \to (X,A)$ and $j\colon (X,A) \to (X,B)$, $k\colon (B,\emptyset) \to (B,A)$ are the inclusions and $\partial$ is determined as
    \[
        \partial\colon  H_n(X,B) \stackrel{\partial}{\longrightarrow} H_{n-1}(B) \stackrel{k_*}{\longrightarrow} H_{n-1}(B,A)
    \]
\end{lemma}
\begin{dremark}
    (Note that $\partial\colon H_n(X,B) \to H_{n-1}(B)$ is just the connecting homomorphism of the pair sequence of $(X,B)$.)
\end{dremark}
