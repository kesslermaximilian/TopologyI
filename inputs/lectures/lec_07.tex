%! TEX root = ../../master.tex
\lecture[]{So 07 Nov 2021}{Untitled}

\begin{orga}
    If you are considering writing a bachelor's thesis in topology, drop an email to Daniel. There will be a presentation of topics in December.
\end{orga}

\begin{lemma}[Homology of disjoint union of spaces]\label{lm:homology-of-disjoint-union-of-spaces}
    Let $I$ be finite and  $\left \{X_i\right\} _{i \in I}$ a family of spaces. Then the inclusions $X_i \to \coprod_{i \in I}X_i$ induce an isomorphism
    \[
        \bigoplus_{i \in I}H_n(X_i) \stackrel{}{\longrightarrow} H_n(\coprod_{i \in I}X_i)
    \] 
\end{lemma}

\begin{oral}
    Note that for $I$ infinite, this was an axiom (disjoint union) a homology theory can satisfy. But the lemma established that for  $I$ finite, this actually holds for all homology theories.
\end{oral}

\begin{proof}
\todo{correct proof}
    We do an induction on $\overline{I}$. Note this is only possible since $I$ is finite. For  $\abs{I} =1$, we just state that the identity induces an isomorphism, which follows from functoriality.

    For the induction step, we already have that
    \[
        \bigoplus_{i \in I}H_n(X_i) \stackrel{\cong}{\longrightarrow} H_n(\coprod_{i \in I'} X_i) \oplus H_n(X_j)
    \] 
    By excision, we get an isomorphism:
    \[
        H_n\left( \coprod_{i \in I'}X_i \right) \stackrel{\cong}{\longrightarrow} H_n\left(\coprod_{i \in I}X_i, X_j\right)
    \] 
    So by the long exact sequence
    \[
    \begin{tikzcd}
        \ar{r}{\partial}[swap]{0} &  H_n(X_j) \ar{r} & H_n(\coprod _{i \in I}X_i) \ar{r}{i_*} &  H_n\left( \coprod_{i \in I}X_i, X_j \right)    \ar{r}{0} &   \ldots  \\
                            & & & H_n\left( \coprod_{i \in I'}X_i \right) \ar{u}{\cong} \ar{ul}
    \end{tikzcd}
\]
splits and thus
\[
    H_n\left( \coprod_{i \in I}X_i \right) \cong H_n(X_j) \oplus H_n\left( \coprod_{i \in I'}X_i \right) 
\] 
\begin{oral}
    Note that in the long exact sequence, we deduce that the boundary maps are the zero maps, as $i_*$ is surjective (remember that the sequence splits), so exactness already means that the kernel of  $\partial$ is the whole space, so that $\partial=0$.
\end{oral}
\end{proof}

\begin{definition}[Well-pointed space]\label{def:well-pointed-space}
    A pointed space $(X,x_0)$ is well-pointed if $(X,x_0)$ is an NDR pair.
\end{definition}

\begin{example}\label{ex:s^n-with-point-is-well-pointed}
    $(S^n, s)$ is well-pointed for all $n\geq 0$ and $s\in S^n$.
\end{example}

\begin{proposition}\label{prop:ndr-pair-induces-isomorphism-on-quotient-space}
    Let $(X,A)$ be an NDR pair. Then the quotient map
    \[
        (X,A) \to (\faktor{X}{A}, \star)
    \] 
    induces an isomorphism
    \[
        H_n(X,A) \stackrel{\cong}{\longrightarrow} H_n(\faktor{X}{A}, \star)
    \] 
    for all $n\in \mathbb{N}$.
\end{proposition}

\begin{dnotation}
    The point $\star$ in the proposition is the collapsed point of  $A$.
\end{dnotation}

\begin{proof}
    Let $U\coloneqq u^{-1}([0,1))$ as in the definition of an NDR-pair.
    Then consider the following diagram:
    \[
    \begin{tikzcd}
        H_n(X,A) \ar[swap]{dd}{} \ar{r}{\cong}[swap]{\text{hom. inv.}} & H_n(X,U) \\
                                                                       & H_n(X\setminus A, U\setminus A)\ar{u}{\cong}[swap]{\text{excision}} \ar{d}{\cong}\\
        H_n(\faktor{X}{A}, \star)  & \ar{l}[swap]{\cong}{\text{excision}}H_n(\faktor{X}{A} \setminus \left \{\star\right\} , \faktor{U}{A}\setminus \left \{\star\right\} )
    \end{tikzcd}
    \]
    Hence, also on the left side we get an isomorphism.
    \todo{Check naturality of this map, i.e. that the isomorphism is really induced by the inclusion}. 
\end{proof}

\begin{remark}
    If $(X,A)$ is an NDR-pair, then  $\left( \faktor{X}{A}, \star \right) $ is well-pointed.
\end{remark}
\begin{proof}
    clear.
\end{proof}

\begin{lemma}\label{lm:wedge-of-well-pointed-spaces-is-also-direct-sum-of-homologies}
    Let $(X_i, x_i)_{i \in I}$ be a family of well-pointed spaces. Assume that $I$ is finite or that  $H$ satisfies the disjoint union axiom. Then
     \[
         H_n(\twedge(X_i,x_i), \star) \cong \oplus_{i \in I}H_n(X_i, x_i)
    \]
    which is induced by the corresponding inclusion maps.
\end{lemma}
\todo{twedge operator in big}

\begin{proof}
    Consider the long exact sequence
    \[
    \begin{tikzcd}
        \ar{r}{\partial} &  H_n(\coprod \left\{x_i\right\} ) \ar{r}{} &  H_n(\coprod X_i) \ar{r}{} &  H_n(\coprod X_i, \coprod \left\{x_i\right\} ) \ar{r}{\partial} &  . \\
        \ar{r}{\partial}&\ar{u}{\cong}   \oplus_{i \in I}H_n(\left\{x_i\right\} ) \ar{r}\ar{u}{\cong} &\ar{u}{\cong} \oplus_{i \in I}H_n(x_i) \ar{r} &\ar[red]{u}{\cong} \oplus_{i \in I} H_n(X_i, \left\{x_i\right\} ) \ar{r}{\partial} &  .
    \end{tikzcd}
\]
The first two isomorphism follow either from \autoref{lm:homology-of-disjoint-union-of-spaces} or from the disjoint union axiom, and by the \nameref{lm:five-lemma} we deduce that also the red map is an isomorphism.
Note that $\left( \coprod X_i, \coprod \left\{x_i\right\}  \right) $ is an NDR pair. Thus by \autoref{prop:ndr-pair-induces-isomorphism-on-quotient-space} we get that
\[
    H_n\left( \coprod X_i, \coprod \left\{x_i\right\}  \right) \cong H_n(\faktor{\coprod X_i}{\coprod \left\{x_i\right\} , \star} =\noloc \twedge_{i \in I} (X_i, x_i)
\] 
\end{proof}

\begin{oral}
    It also holds in general that the disjoint union of NDR pairs is again an NDR pair. One just constructs the desired maps component-wise.
\end{oral}

\begin{lemma}\label{lm:induced-homology-of-general-linear-map-of-sphere}
    Assume that $H$ satisfies the dimension axiom. Then:
    \begin{enumerate}[h]
        \item For $n\in \mathbb{Z}$, set
                \begin{equation*}
                f_n: \left| \begin{array}{c c l} 
                S^1 & \longrightarrow & S^1 \\
                z & \longmapsto &  z^n
                \end{array} \right.
            \end{equation*}
            Then $(f_n)_*\colon H¹(S^1) \to H_1(S^1)$ is given by multiplication with $n$. 
        \item For $A\in \GL_{d+1}(\mathbb{R})$, $d\geq 1$, we have an induced map
                \begin{equation*}
                \begin{array}{c c l} 
                S^d & \longrightarrow & S^d \\
                x & \longmapsto &  \frac{Ax}{\norm{Ax}}
                \end{array}
            \end{equation*}
            Then $(f_A)_*\colon H_d(S^d) \to H_d(S^d)$ is given by multiplication with $\frac{\det A}{\norm{\det A} }$
    \end{enumerate}
\end{lemma}

\begin{proof}
    Note that for 
    \[
        \GL_{d+1}(\mathbb{R})\subset \mathcal{M}_{d+1}(\mathbb{R})\cong \mathbb{R}^{(d+1)^2}
    \] 
    with the subspace topology,
    \[
        A \mapsto \frac{\det A}{\norm{\det (A)} }
    \] 
    is continuous and induces a bijection $\pi_0(\GL_{d+1}(\mathbb{R})) \to \left\{\pm 1\right\} $.
    \begin{oral}
        Usually, this is done in some course on analysis, so this will not be done here. It's just standard analysis to check that the determinant is indeed continuous.
    \end{oral}
    So by homotopy invariance, we only have to consider $A = \id$ and

    \[
        B =\begin{pmatrix} -1 & 0 & \ldots\\ 0 & 1 & 0 & \ldots \\ \ldots \\ \ldots & 0 & 1\end{pmatrix}
    \]
    Since $f_{\id} = \id$, $\left( f_{\id} \right) _\star = \id$ as claimed.
    Note that $f_B = \sum^d τ$ for  $τ\colon S^0 \to S^0$, where $τ$ flips the two points of  $S^0$.
    By the suspension isomorphism (\autoref{thm:suspension-shifts-homology-by-1}), it suffices to show that
    \[
        τ_\cdot \colon  \tilde{H}_0(S^0) \to \tilde{H}_0(S^0)
    \] 
    is given by multiplication with $-1$, as
    \[
    \begin{tikzcd}
        \tilde{H}_0(S^0) \ar[swap]{d}{\cong} \ar{r}{τ_*} & \tilde{H}_0(S^0) \ar{d}{\cong} \\
        H_d(S^d) \ar[swap]{r}{(f_B)_*} & H_d(S^d)
    \end{tikzcd}
    \]
    commutes. 
    \begin{oral}
        Note that here we used the dimension axiom, as otherwise, in the diagram we would need to fully work with the reduced homology.
    \end{oral}
    So now consider
    \[
    \begin{tikzcd}
        0 \ar{r} & \ar{d}{τ_*}\tilde{H}_0  (S^0)\ar{r} & \ar{d}{τ_*}H_0(S^0) \ar{r} & H_0(\star)\ar{r} \ar{d}{\id}& 0 \\
                 & \tilde{H}_0(S^0) \ar{r} & H_0(S^0) \ar{r} & H_0(\star) \ar{r} & 0
    \end{tikzcd}
\]
as $H_0(S^0) \cong \mathbb{Z} \oplus \mathbb{Z}$ (or more generally: $R \oplus R$), the diagram is isomorphic to the following diagram:
\[
    \begin{tikzcd}[ampersand replacement=\&]
        0 \ar{r} \&  \ar{d}{τ_*}\mathbb{Z} \ar{r}{(\id, -\id)} \&  \ar{d}{\begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} }\mathbb{Z} \oplus \mathbb{Z} \ar{r}{+} \&  \mathbb{Z} \ar{r} \&  0 \\
     0 \ar{r} \&  \mathbb{Z} \ar{r}{(\id,-\id)} \&  \mathbb{Z} \oplus \mathbb{Z} \ar{r}{+} \&  \mathbb{Z} \ar{r} \& 0
\end{tikzcd}
\]
so that it follows $τ_* = -\id$.

It remains to show part 1). By part 2), $(f_{-1})_* = ( f_{\scalebox{0.5}{$\begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}$} } )_* = -\id $. But now, as
\[
    (f_{-n})_* = (f_{-1} \circ  f_n)_* = (f_{-1})_* \circ (f_n)_* = -(f_n)_*
\] 
Thus it suffices to show 1) for $n\geq 2$.
Consider the pinch map $S^1 \to \twedge_{i=1}^n S^1$.

\missingfigure{pinch map}

Let $p_i\colon \twedge S^1 \to S^1$ be the map that sends all but the $i$th factor to a point. Then $p_i \circ  g \simeq \id_{S^1}$.
Let $h\colon  \twedge S^1 \to S^1$ be the map that identifies all copies with $S^1$. Then $h \circ  g = f_n$.
But now we can consider the following diagram:
\[
\begin{tikzcd}
    H_1(S^1) \ar{dr}{\Delta}\ar{r}{g_*}\ar[bend left = 30]{rr}{(f_n)_*} &  H_1(\twedge_{i=1}^n S^1) \ar{r}{h_*} &  H_1(S^1)\\
                                                                    & \bigoplus_{i=1}^n H_1(S^1) \ar{u}{\cong}\ar{ur}{+}
\end{tikzcd}
\]
Note that the left square commutes because projecting onto the $i$th factor, i.e.  $H¹(S^1)$ yields identities, and the isomorphism in the middle is just induced by the inclusions.
But now
\[
    z\stackrel{\Delta}{\longmapsto} (z,\ldots,z) \stackrel{+}{\longmapsto} nz
\]
establishes our desired result.
\end{proof}

\begin{definition}
    A \vocab{vector field} $v$ on  $S^d$ is a map  $v\colon S^d \to \mathbb{R}^{d+1}$ such that
    \[
        \forall x\in S^d \qquad \left< x, v(v) \right> =0
    \] 
    $v$ is called \vocab{nowhere vanishing} if $v(x) \neq 0$  for all $x$.
\end{definition}

\missingfigure{tangent space}

\begin{theorem}[Combing the hedgehog]\label{thm:combing-the-hedgehog-s-d-has-nowhere-vanishing-vector-field-iff-d-is-odd}
    $S^d$ admits a nowhere vanishing vector field if and only if  $d$ is odd.
\end{theorem}
\begin{proof}
    For $d = 2n-1$, we set
     \[
         x = (x_1,\ldots,x_{2n}) \stackrel{v}{\longmapsto} (x_2, -x_1, x_4, -x_3, \ldots, x_{2n}, -x_{2n-1})
    \] 
    This is a nowhere vanishing vector field. Now assume that such $v$ exists. Define a homotopy from $\id_{S^n}$ to $-\id_{S^n}$ (antipodal map) as follows:

    \missingfigure{homotopy on $S^n$}
    \begin{oral}
        One can describe this explicitly in terms of $\sin, \cos$ and check that this is indeed a homotopy, but we will not do this here.
    \end{oral}
    Then $(\id)_* = \id$ equals
     \[
         \det(-\id) = \left(f_{\scalebox{0.5}{
                     $
         {
             \begin{pmatrix}
                 -1 & 0 & \ldots \\
                  \ldots  &\vdots & \ldots\\
                \ldots&0 & -1
        \end{pmatrix}
        }$
         }
         }\right) 
    \] 
    But $\det(-\id) = (-1)^{d+1}$, so to be $1$,  $d$ has to be odd.
\end{proof}
