%! TEX root = ../../master.tex
\lecture[]{Sa 20 Nov 2021}{Untitled}

\begin{editor}
    In the beginning of the lecture, there have been some corrections / additions regarding \autoref{thm:mayer-vietoris} and in particular \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad}.
    In particular, have a look at the new \autoref{def:h-star-isomorphism} that is now used instead of the notion of an excisive triad. 
\end{editor}

\subsection{Excision}

\begin{theorem}\label{thm:singular-homology-satisfies-excision}
    Let $A\subset B\subset X$ such that $\overline{A} \subset \mathring{B}$.
    Then the inclusion induces an isomorphism
    \[
        H_n^{\sing}(X\setminus A, B\setminus A) \to H_n^{\sing}(X,B) 
    \] 
    for all $n\in \mathbb{N}$.
\end{theorem}

\missingfigure{idea of excision / centric subdivision}

So our goal is to subdivide a given simplex $α\colon \Delta^n \to  X$ such that all pieces lie in $X\setminus A$ or in $B$.
This is known as centric subdivision.

\begin{definition}\label{def:barycenter}
    The \vocab{barycenter} $b(\Delta^n)$ of $\Delta^n$ is the point
    \[
        \left( \frac{1}{n+1},\ldots,\frac{1}{n+1} \right) 
    \] 
\end{definition}

    Given any sequence
    \[
        S \coloneqq  (S_0\subset S_1 \subset \ldots\subset S_{n-1}\subset \Delta^n)
    \] 
    where $s_i$ is an  $i$-face of  $\Delta^n$ we obtain $α_s \in C_n^{\sing(\Delta^n)}$ by
    \[
        k \mapsto b(s_k)
    \] 

\begin{example}
    The sequence $\left\{1\right\} \subset \left\{0,1\right\} \subset \left\{0,1,2\right\} $ yields the following:

    \missingfigure{illustration-of-centric-subdivision-of-2-simplex}
\end{example}

Adding all such subsimplices with the correct signs, we obtain an element in $C_n^{\sing}(\Delta^n)$ that differs from $\id_{\Delta^n}$ by a boundary.

To get the signs correct, we make the following inductive definition:

\begin{definition}
    \begin{enumerate}[1)]
        \item Let $X\subset \mathbb{R}^m$ be convex,  $x\in X$.
            Then the cone construction $C_{x_0}$ is given by
                \begin{equation*}
                C_{x_0}: \left| \begin{array}{c c l} 
                    C_n^{\sing}(X) & \longrightarrow & C_{n+1}^{\sing}(X) \\
                    α & \longmapsto &  (-1)^{n+1} \left( k \mapsto \begin{cases}
                            α(k) & k \leq n \\
                            x_0 & k = n+1
                    \end{cases} \right) 
                \end{array} \right.
            \end{equation*}
        \item Define the \vocab{barycentric subdivision}
                \begin{equation*}
                B_*^X: \left| \begin{array}{c c l} 
                    C_*^{\sing}(X) & \longrightarrow & C_*^{\sing}(X) \\
                B_0^X & \longmapsto &  \id \\
                B_n^X(α) &\longmapsto & α_*\left( C_{b(\Delta^n)}\left( B_{n-1}^{\Delta^n}(\partial \id_{\Delta^n} \right)  \right) 
                \end{array} \right.
            \end{equation*}
            
    \end{enumerate}
\end{definition}

\begin{abuse}
    Note that this is formally not totally correct.
    Although we linearly extend from the edges of $α$ to  $x_0$, we want to keep the property $c_{x_0}(α)\circ δ^{n+1} = α$.
\end{abuse}

\begin{example}
    \begin{enumerate}[1.]
        \item  $B_1^{\Delta^1}(\id_{\Delta^1}) = C_{b(\Delta^1)}(\partial\id_{\Delta^1})$
            \missingfigure{construction}
        \item 
            \[
                B_1^{\Delta^2}(δ^i) = δ^i_*(B_1^{\Delta^1}(\id_{\Delta^1}))
            \]
            and thus
            \[
                B_2^{\Delta^2}(\id_{D^2}) = C_{b(D^2)} (B_1^{\Delta^2}(\partial\id_{\Delta^2}))
            \] 
            \missingfigure{Subdivision of 2-simplex}
    \end{enumerate}
\end{example}

\begin{lemma}\label{lm:barycentric-subdivision-is-a-chainmap}
    $B_*$ is a natural chain map.
\end{lemma}
\begin{proof}
    Naturality follows directly from the definition, i.e. for a map $f\colon X \to Y$, then
    \[
        B_n^Y(f_*α) = f_*(α_*(C_{b(\Delta^n)}(B_{n-1}^{\Delta^n}(\partial\id_{\Delta^n})) = f_*(B_n^X(α)) 
    \]
    We first observe that (for convex $X,x_0\in X$) we have that
    \[
        \partial C_{x_0}(α) = -C_{x_0}(\partialα) + α
    \]
    as leaving out one of the first $n$ vertices, we get the cone of the rest, and leaving out the last vertex, we get  $(-1)^{n+1}α$.
    Note that the $(-1)^{n+1}$ cancels with the sign of the cone construction.

    Now, we show our claim by induction.
    For $n=0$, we have  $\partial B_0^X(α) = \partial α = 0$

    For the induction step $n-1 \mapsto n$, we have for any $α\colon \Delta^n \to  X$
     \begin{IEEEeqnarray*}{rCl}
         \partial(B_n^X(α) & = & \partial(α_*(C_{b(\Delta^n)}(B_{n-1}^{\Delta^n}(\partial\id_{\Delta^n}))) \\
                           & \stackrel{\text{$α_*$ is a chain map}}{=}  & α_*(\partial(C_{b(\Delta^n)}(B_{n-1}^{\Delta^n}(\partial\id_{\Delta^n}))) \\
                           & \stackrel{\text{claim}}{=}  & α_*(-C_{b(\Delta^n)}(\partial B_{n-1}^{\Delta^n}(\partial\id_{\Delta^n})) + B_{n-1}^{\Delta^n}(\partial\id_{\Delta_n})) \\
                           & \stackrel{\text{$B_{n-1}^{\Delta^n}$ is a chain map}}{=}  & α_*(-C_{b(\Delta^n)}(B_{n-1}^{\Delta^n}(\partial\partial \id_{\Delta^n})) + B_{n-1}^{\Delta^n}(\partial \id_{\Delta^n})) \\
                           & = &  α_*(B_{n-1}^{\Delta^n}(\partial\id_{\Delta^n})) \\
                           & \stackrel{\text{$B_{n-1}^{\Delta}$ is natural}}{=}  & B_{n-1}^{X}(α_*(\partial\id_{\Delta^n})) \\
                           & = & B_{n-1}^{X}(\partialα)
    \end{IEEEeqnarray*}
    
\end{proof}

\begin{definition}\label{def:chain-homotopoy-for-excision-of-singular-homology}
    Define $H_*\colon C_*^{\sing}(X) \to C_{\star +1}^{\sing}(X)$ inductively by $H_0 = 0$ and
     \[
         H_n(α) = α_*\left( C_{b(\Delta^n)}(B_n(\id_{\Delta^n}) - \id_{\Delta^n} - H_{n-1}(\partial\id_{\Delta^n})) \right) 
    \] 
\end{definition}

\begin{lemma}\label{lm:subdivision-gives-rise-to-chain-homotopy}
    $H_*$ is a chain homotopy between  $\id$ and $B_*$.
\end{lemma}

\begin{proof}
    exercises.
\end{proof}

\begin{lemma}\label{lm:barycentric-subdivision-makes-subsimplices-small}
    Every simplex of $B_n(\id_{\Delta^n})\in C_n^{\sing}(\Delta^n)$ has diameter $\leq \frac{n}{n+1}\diam (\Delta^n)$.
\end{lemma}

\begin{proof}
    By symmetry it suffices to show this for the simplex given by $\left\{0\right\} \subset \left\{0,1\right\} \subset \left\{0,1,2\right\} \subset \ldots$.
    The diameter is always realized by the distance between two vertices, as the simplex is compact (so the diameter is attained as the difference of two points), we can assume these lie on the boundary (if not, join the two points by a line and intersect with the boundary), and finally we can move to the vertices, as this will increase the distance.

    By induction, we also see that these two points involve $b(\Delta^n)$, so as $d(b(\Delta^n)), δ^i)$ is realized by
    \[
        d(b(\Delta^n), j)
    \] 
    for some vertex of $\Delta^n$, the diameter is given by
    \begin{IEEEeqnarray*}{Cl}
&        \norm{(1,0,\ldots,0) - \left( \frac{1}{n+1}, \ldots, \frac{1}{n+1} \right) } \\
        = & \sqrt{\left( \frac{n}{n+1} \right) ^2 + n\left( \frac{1}{n+1} \right) ^2} \\
        = & \sqrt{\frac{n(n+1)}{(n+1)^2}} = \sqrt{\frac{n}{n+1}}
    \end{IEEEeqnarray*}
    But as
    \[
        \diam(\Delta^n) = \norm{(1,0,\ldots,0) - (0,1,0,\ldots)} = \sqrt{2}
    \] 
    we see that
    \[
        \frac{n}{n+1}\sqrt{2} \geq  \frac{n}{n+1} \sqrt{\frac{n+1}{n}} = \sqrt{\frac{n}{n+1}}
    \] 
\end{proof}

\begin{corollary}\label{cor:subdivision-gives-arbitrary-small-simplices}
    $\forall ε>0$, there exists $k\in \mathbb{N}$ such that every simplex in $B^k_n(\id_{\Delta^n}))$ (applying $B_n$  $k$ times) has diameter  $<ε$.
\end{corollary}

\DeclareSimpleMathOperator{small}

\begin{refproof}{thm:singular-homology-satisfies-excision}
    Let $C_n^{\small}(X)\subset C_n^{\sing}(X)$ consist of the subspace generated by all maps $α\colon \Delta^n \to X$ that have image in $X\setminus A$ or in $B$. 

    \begin{claim}
        The inclusion $ι_*\colon C_n^{\small} \to C_n^{\sing}$ induces an isomorphism in homology.
    \end{claim}

    Let $x\in C_n^{\sing}(X)$ represent a homology class.
    Since $x$ is a finite sum of maps  $α_i\colon \Delta^n \to X$, $\Delta^n$ compact and 
    \[
    X = X\setminus \overline{A} \cup \mathring{B}
    \] 
    is an open cover, by \autoref{cor:subdivision-gives-arbitrary-small-simplices} there exists $k\in \mathbb{N}$ such that $B_n^k(x) \in C_n^{\small}(X)$.

    By \autoref{lm:subdivision-gives-rise-to-chain-homotopy} , $[B_n^k(x)] = [x] \in H_n^{\sing}(X)$, $i_*$ is surjective.

    To prove injectivity, let  $a\in C_n^{\small}(X)$ be such that $[i_*a] = 0\in H_n^{\sing}(X)$.
    Then there exists $b\in C_{n+1}^{\sing}(X)$ with $\partial b = a$.
    Again, then $\exists k'$ with $B^{k'}(b) \in C_{n}^{\small}(X)$.
    Then $\partial B^{k'}(b) = B^{k'}(\partial b) = B^{k'}(a)$.

Note that
\[
    C_n^{\small}(X) \cong \faktor{C_n^{\sing}(X \setminus A) \oplus C_n^{\sing}(B)}{C_n^{\sing}(X\setminus A \cap B)}
\]
Let $a = a_1 + a_2$ with $a_1 \in C_n^{\sing}(X\setminus A)$ and $a_2\in C_n^{\sing}(B)$.
Then by naturality of $B^{k'}$ we have
\[
    B^{k'}(a) = B^{k'}(a_1) + B^{k'}(a_2)
\]
and $B^{k'}$ is homotopic to $\id$ in  $C^{\sing}(X\setminus A)$ and $C^{\sing}(B)$.
Thus $B^{k'}$ is homotopic to $\id$ in  $C_n^{\small}(X)$ and thus
\[
    0 = [\partial B^{k'}(B)] = [B^{k'}(a)] = [a] \in H_n(C_n^{\small}(X))
\] 

\end{refproof}
