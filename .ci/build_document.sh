set -e
echo "Building document"
make init-git-hooks
make 2021_Topology_I-with-gnuplots
make gnuplots-master
touch 2021_Topology_I.gnuplots/.gitkeep
touch master.gnuplots/.gitkeep
mkdir gnuplots
mv 2021_Topology_I.gnuplots gnuplots
mv master.gnuplots gnuplots
mkdir public
mv 2021_Topology_I.pdf public/
mv 2021_Topology_I.log public/
cd public/
tree -H '.' -I "index.html" -D --charset utf-8 -T "Topology I" > index.html
